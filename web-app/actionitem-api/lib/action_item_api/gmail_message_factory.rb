module ActionItemApi
  module GmailMessageFactory
    def self.build(gmail_message, tracked_item)
      tracked_item.messages.create(
        provider_message_id: gmail_message.gmail_id,
        deep_link: 'www.google.com/search?q=replace+with+actual+deeplink',
        body: gmail_message.body,
        provider_date_posted: gmail_message.date
      )
    end
  end
end
