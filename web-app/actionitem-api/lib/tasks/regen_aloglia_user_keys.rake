desc 'Regenerate user algolia keys'

task regen_aloglia_user_keys: :environment do
  User.all.each(&:regen_algolia_key!)
end
