desc 'Refresh'

task refresh_user_watch_access: :environment do
  User.all.each(&:refresh_provider_watches)
end
