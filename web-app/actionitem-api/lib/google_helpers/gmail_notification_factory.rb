require 'ostruct'

module GoogleHelpers
  module GmailNotificationFactory
    def self.build(params)
      OpenStruct.new(
        email: decode_data(params)['emailAddress'],
        history_id: decode_data(params)['historyId']
      )
    end

    def self.decode_data(params)
      JSON.parse(Base64.decode64(params[:message][:data]))
    end
  end
end
