module GoogleHelpers
  class GmailGetHistory < GoogleBase
    attr_reader :start_history_id

    def post_initialize(**opts)
      @start_history_id = opts[:start_history_id] || not_implemented(:start_history_id)
    end

    def call
      client.list_user_histories(
        'me',
        start_history_id: start_history_id
      )
    end

    private

    def client
      GoogleClient.call(google_credentials: google_credentials)
    end
  end
end
