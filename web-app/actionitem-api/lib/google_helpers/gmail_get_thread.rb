module GoogleHelpers
  class GmailGetThread < GoogleBase
    attr_reader :thread_id

    def post_initialize(**opts)
      @thread_id = opts[:thread_id] || default_thread_id
    end

    def call
      get_thread(@thread_id)
    end

    private

    def get_thread(thread_id)
      client.get_user_thread('me', thread_id)
    end

    def client
      GoogleClient.call(google_credentials: google_credentials)
    end

    def default_thread_id
      not_implemented(:thread_id)
    end
  end
end
