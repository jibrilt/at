require 'google/apis/gmail_v1'

module GoogleHelpers
  class GoogleBase
    attr_accessor :google_credentials

    def initialize(**opts)
      @google_credentials = opts[:google_credentials] || not_implemented(:google_credentials)
      post_initialize(opts)
    end

    def post_initialize(opts); end

    def call; end

    def self.call(**opts)
      new(**opts).call
    end

    private

    def not_implemented(attribute)
      raise NotImplementedError, I18n.t('lib.missing_attribute', attr: attribute)
    end
  end
end
