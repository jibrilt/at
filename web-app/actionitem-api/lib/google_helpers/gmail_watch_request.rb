module GoogleHelpers
  class GmailWatchRequest < GoogleBase
    attr_reader :topic_name

    def post_initialize(**opts)
      @topic_name = opts[:topic_name] || default_topic_name
    end

    def call
      watch_request = watch_via_topic(@topic_name)
      client.watch_user('me', watch_request)
    end

    private

    def watch_via_topic(topic_name)
      watch_request = Google::Apis::GmailV1::WatchRequest.new
      watch_request.topic_name = topic_name
      watch_request
    end

    def client
      GoogleClient.call(google_credentials: google_credentials)
    end

    def default_topic_name
      ENV['GMAIL_INBOX_WATCH_TOPICNAME']
    end
  end
end
