require 'google/apis/gmail_v1'

module GoogleHelpers
  class GoogleClient < GoogleBase
    delegate :access_token,
             :refresh_token, :token_expired?, to: :google_credentials

    def call
      renew_access_token if token_expired?

      client = signet_client(access_token)
      gmail = gmail_service
      gmail.authorization = client
      gmail
    end

    private

    def gmail_service
      Google::Apis::GmailV1::GmailService.new
    end

    def signet_client(access_token)
      Signet::OAuth2::Client.new(
        access_token: access_token
      )
    end

    def renew_access_token
      # TO DO: This must be refactored along with Oauth (see issue #12)
      resp = ActionItem::Google::Oauth.new.refresh_access_token(refresh_token).to_h.deep_symbolize_keys
      raise 'no access token' if resp[:access_token].blank?

      google_credentials.user.gmail_provider.update(token: resp[:access_token])
      @google_credentials = GmailCredentialsFactory.build(google_credentials.user)
    end
  end
end
