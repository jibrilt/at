module GoogleHelpers
  class GmailGetMessage < GoogleBase
    attr_reader :message_id, :format

    def post_initialize(**opts)
      @message_id = opts[:message_id] || default_message_id
      @format = opts[:format]
    end

    def call
      get_message(message_id, format)
    end

    private

    def get_message(message_id, format)
      client.get_user_message('me', message_id, format: format)
    end

    def client
      GoogleClient.call(google_credentials: google_credentials)
    end

    def default_message_id
      not_implemented(:message_id)
    end
  end
end
