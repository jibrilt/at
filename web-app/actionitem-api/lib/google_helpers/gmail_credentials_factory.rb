require 'ostruct'

module GoogleHelpers
  module GmailCredentialsFactory
    def self.build(user)
      OpenStruct.new(
        user: user,
        access_token: user.gmail_access_token,
        refresh_token: user.gmail_refresh_token,
        token_expired?: user.gmail_access_token_expired?
      )
    end
  end
end
