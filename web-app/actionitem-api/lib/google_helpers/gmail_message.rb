module GoogleHelpers
  class GmailMessage
    attr_accessor :gmail_message
    attr_reader :gmail_id, :headers

    delegate :thread_id, :payload, to: :gmail_message

    def initialize(**opts)
      @gmail_message = opts[:gmail_message] || not_implemented(:gmail_message)
      @gmail_id = gmail_message.id
      @headers = payload.headers
    end

    def subject
      parse_header('Subject')
    end

    def body
      if multipart?
        text_parts = get_text_parts(payload.parts)
        text_parts.map { |part| stringify_body(part) }.join
      else
        stringify_body(payload.body.data)
      end
    end

    def date
      parse_datetime(parse_header('Date'))
    end

    def from
      parse_header('From')
    end

    def to
      parse_header('To')
    end

    def multipart?
      payload.parts.any?
    end

    protected

    def force_utf_encoding(string)
      string&.force_encoding('UTF-8')
    end

    def parse_datetime(string)
      DateTime.parse(string)
    end

    private

    def stringify_body(part)
      body = part.body.data
      force_utf_encoding(body).squish
    end

    def get_text_parts(parts)
      text_parts = identify_text_parts(parts)
      identify_multiparts(parts).each do |multipart|
        text_parts << get_text_parts(multipart.parts)[0]
      end
      text_parts
    end

    def identify_text_parts(parts)
      parts.select { |part| text_extension?(part) }
    end

    def identify_multiparts(parts)
      parts.select { |part| multipart_extension?(part) }
    end

    def text_extension?(part)
      part.mime_type == 'text/plain'
    end

    def multipart_extension?(part)
      part.mime_type.start_with?('multipart')
    end

    def parse_header(name)
      headers.find { |h| h.name == name }&.value
    end

    def not_implemented(attribute)
      raise NotImplementedError, I18n.t('lib.missing_attribute', attr: attribute)
    end
  end
end
