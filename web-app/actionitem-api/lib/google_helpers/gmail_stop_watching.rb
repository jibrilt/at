module GoogleHelpers
  class GmailStopWatching < GoogleBase
    def call
      client.stop_user('me')
    end

    private

    def client
      GoogleClient.call(
        google_credentials: google_credentials
      )
    end
  end
end
