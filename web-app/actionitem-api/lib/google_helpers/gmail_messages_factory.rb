module GoogleHelpers
  module GmailMessagesFactory
    def self.build(messages)
      messages.map { |m| GmailMessage.new(gmail_message: m) }
    end
  end
end
