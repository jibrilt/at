module SlackHelpers
  module BlockKit

    # Slack types

    def slack_section_text(text)
      {
        type: :section,
        text: {
          type: :mrkdwn,
          text: text
        }
      }
    end

    def slack_section_fields(*fields)
      {
        type: :section,
        fields: [
          *fields
        ]
      }
    end

    def slack_context(*fields)
      {
        type: :context,
        elements: [
          *fields
        ]
      }
    end

    def slack_divider
      {
        type: :divider
      }
    end

    # Slack Fields

    def slack_markdown_field(text)
      {
        type: :mrkdwn,
        text: text
      }
    end

    # Slack Action

    def slack_action_fields(*fields)
      {
        type: :actions,
        elements: [
          *fields
        ]
      }
    end

    def slack_button(
      style: :primary,
      text:,
      value: nil
    ) {
        type: :button,
        style: style,
        text: {
          type: :plain_text,
          text: text
        },
        value: value
      }
    end
  end
end
