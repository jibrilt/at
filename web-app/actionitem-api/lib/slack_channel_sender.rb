class SlackChannelSender < SlackWrapper
  attr_accessor :blocks, :channel

  def post_initialize(**opts)
    @blocks = opts[:blocks] || default_blocks
    @channel = opts[:channel] || default_channel
  end

  def self.send_with(**opts)
    new(**opts).send_by_blocks
  end

  def send_by_blocks
    client.chat_postMessage(
      channel: channel,
      blocks: blocks
    )
  end

  private

  def default_blocks
    not_implemented(:blocks)
  end

  def default_channel
    not_implemented(:channel)
  end

  def default_slack_token
    not_implemented(:slack_token)
  end

  def not_implemented(attr)
    raise NotImplementedError, t('lib.missing_attribute', attr: attr)
  end
end
