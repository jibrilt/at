class SlackWrapper
  attr_accessor :slack_token

  def initialize(**opts)
    @slack_token = opts[:slack_token] || default_slack_token

    post_initialize(**opts)
  end

  def post_initialize(**opts); end

  protected

  def client
    Slack::Web::Client.new(
      client_id: slack_client_id,
      client_secret: slack_client_secret,
      token: slack_token
    )
  end

  private

  def slack_client_secret
    ENV.fetch('SLACK_CLIENT_SECRET')
  end

  def slack_client_id
    ENV.fetch('SLACK_CLIENT_ID')
  end

  def default_slack_token; end
end
