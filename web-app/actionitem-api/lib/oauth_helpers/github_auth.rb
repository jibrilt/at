module OauthHelpers
  class GithubAuth
    include Rails.application.routes.url_helpers

    attr_accessor :code

    def initialize(code:)
      @code = code
    end

    def get_access_token
      client = OAuth2::Client.new(
        client_id,
        client_secret,
        site: site,
        token_url: token_url
      )

      client
        .auth_code
        .get_token(
          code
        ).token
    end

    def client_id
      ENV.fetch('VUE_APP_GITHUB_CLIENT_ID')
    end

    def client_secret
      ENV.fetch('GITHUB_CLIENT_SECRET')
    end

    def site
      'https://github.com'
    end

    private

    def token_url
      '/login/oauth/access_token'
    end
  end
end
