module GmailTracking
  class PushNotificationsController < ApplicationController
    def create
      notification = build_notification(params)
      user = find_user(notification.email)
      return if user.blank?

      publish_gmail_notification(notification, user)
    end

    private

    def publish_gmail_notification(notification, user)
      GmailPublishNotification.call(
        notification: notification,
        user: user
      )
    end

    def build_notification(params)
      GmailNotificationFactory.build(params)
    end

    def find_user(email)
      User.find_by_email(email)
    end
  end
end
