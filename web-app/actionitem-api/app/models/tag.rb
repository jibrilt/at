class Tag < ApplicationRecord
  include GqlNodeHelper

  validates :name,
    presence: true,
    uniqueness: {
      scope: [:tracked_item_id],
      case_sensitive: false
    }

  validates :tracked_item_id,
    presence: true

  belongs_to :tracked_item, optional: false
  belongs_to :user, optional: true

  before_validation :downcase_name
  before_create :set_user

  after_create :broadcast_to_frontend
  after_destroy :broadcast_to_frontend

  private

  def broadcast_to_frontend
    BroadcastTrackedItemList.call(
      tracked_item: tracked_item,
      user: user,
      event_type: 'trackeditem update'
    )
  end

  def set_user
    self.user = tracked_item.user
  end

  def downcase_name
    self.name = name&.downcase
  end
end
