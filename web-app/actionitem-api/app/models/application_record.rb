class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  private

  def custom_error_message(label)
    tag = "#{self.class.name.downcase}.#{label}"

    I18n.t("activerecord.errors.models.#{tag}")
  end
end
