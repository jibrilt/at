class OauthProvider < ApplicationRecord
  include ProviderMarkable

  belongs_to :user
end
