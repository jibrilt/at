class GmailMessageHistory < ApplicationRecord
  include GoogleHelpers

  belongs_to :user

  validates :user_id, presence: true
  validates :history_id, presence: true

  scope :descending, -> { order(created_at: :desc) }

  scope :push_notifications, lambda { |user|
    where(user: user, push_notification: true)
  }

  def self.second_to_the_last(target_user)
    where(user: target_user).descending.third
  end
end
