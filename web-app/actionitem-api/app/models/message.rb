class Message < ApplicationRecord
  include GqlNodeHelper
  include AlgoliaSearch

  before_save :set_provider_date_posted_i

  after_create :update_tracked_item_latest_date,
    :broadcast_to_frontend

  after_touch :index!

  belongs_to :tracked_item

  validates :tracked_item_id, presence: true
  validates :provider_message_id, presence: true
  validates :deep_link, presence: true
  validates :body, presence: true
  validates :provider_date_posted, presence: true

  scope :by_provider_date_posted, -> {
    order(provider_date_posted: :desc)
  }

  algoliasearch disable_indexing: Rails.env.test? do
    attributes :tracked_item_id,
      :provider_message_id,
      :deep_link,
      :body,
      :provider_date_posted,
      :created_at,
      :gql_id,
      :provider_date_posted_i

    attribute(:user_id) do
      tracked_item.user.id
    end

    attribute(:provider) do
      tracked_item.provider
    end

    attribute(:from) do
      tracked_item.initial_from
    end

    attribute(:from_email) do
      tracked_item.initial_from_email
    end

    attribute(:subject) do
      tracked_item.initial_subject
    end

    attribute(:provider_date_posted_i) do
      provider_date_posted.to_i
    end

    attribute(:notification_text) do
      tracked_item.unread_notifications_to_s
    end

    attribute(:tracked_item_gql_id) do
      tracked_item.gql_id
    end
  end

  private

  def update_tracked_item_latest_date
    tracked_item
      .update!(
        latest_message_provider_date: provider_date_posted
      )
  end

  def broadcast_to_frontend
    MessageBroadcastProcessor.call(
      tracked_item: tracked_item,
      messages: tracked_item.messages,
      user: tracked_item.user
    )
  end

  def set_provider_date_posted_i
    self.provider_date_posted_i = provider_date_posted.to_i
  end
end
