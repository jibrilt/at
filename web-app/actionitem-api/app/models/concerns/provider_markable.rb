module ProviderMarkable
  extend ActiveSupport::Concern

  PROVIDER_WHITELIST = [
    'slack', 'gmail','github',
    'asana', 'outlook', 'jira',
    'teams', 'todoist'].freeze

  included do
    validates :provider, presence: true, inclusion: { in: PROVIDER_WHITELIST }

    PROVIDER_WHITELIST.each do |whitelisted_provider|
      define_method "#{whitelisted_provider}?" do
        provider == whitelisted_provider
      end
    end

    def email_provider?
      ['gmail', 'outlook'].member?(provider)
    end

    def messaging_provider?
      ['slack', 'teams'].member?(provider)
    end

    def posting_provider?
      ['github', 'jira', 'todoist'].member?(provider)
    end

    class << self
      PROVIDER_WHITELIST.each do |whitelisted_provider|
        define_method whitelisted_provider do
          where(provider: whitelisted_provider)
        end
      end
    end
  end
end
