class TrackedItem < ApplicationRecord
  include GqlNodeHelper
  include ProviderMarkable

  belongs_to :user, optional: false

  has_many :messages, dependent: :destroy
  has_many :tracked_item_notifications, dependent: :destroy
  has_many :tags, dependent: :destroy

  after_touch { messages.each(&:touch) }

  validates :user_id, presence: true
  validates :provider_thread_id, presence: true
  validates :initial_from_email, presence: true
  validates :initial_from, presence: true
  validates :initial_subject, presence: true

  scope :by_latest_message, -> {
    order(latest_message_provider_date: :desc)
  }

  alias_attribute :notifications, :tracked_item_notifications

  def latest_message
    messages.by_provider_date_posted.first
  end

  def unread_notifications_to_s
    unread_ti_notifications(self)
  end

  private

  def unread_ti_notifications(target_tracked_item)
    UnreadTrackeditemNotifications.call(
      tracked_item: target_tracked_item
    )
  end
end
