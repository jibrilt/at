class TrackedItemNotification < ApplicationRecord
  include AASM
  include GqlNodeHelper

  NOTIFICATION_TYPES = [
    'message', 'email', 'reaction', 'post'
  ].freeze

  belongs_to :tracked_item

  validates :notification_type,
    presence: true,
    inclusion: { in: NOTIFICATION_TYPES }

  validates :tracked_item_id, presence: true
  validates :aasm_state, presence: true

  scope :unread_count, -> {
    unread
      .group(:notification_type)
      .count(:notification_type)
  }

  aasm do
    state :unread, initial: true
    state :read

    event :mark_as_read do
      transitions from: :unread, to: :read
    end
  end
end
