class User < ApplicationRecord
  include GqlNodeHelper

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :trackable, :lockable, :timeoutable,
         :token_authenticatable # , :omniauthable

  has_many :tracked_items, dependent: :destroy
  has_many :gmail_message_history, dependent: :destroy

  has_many :oauth_providers, dependent: :destroy
  has_many :tags

  after_create :regen_algolia_key!

  TrackedItem::PROVIDER_WHITELIST.each do |provider|
    # eg: gmail_providers
    define_method "#{provider}_providers" do
      oauth_providers.where(provider: provider)
    end
  end

  def github_access_token
    github_providers
      .first
      .access_token
  end

  def refresh_provider_watches
    RefreshProviderWatches.call(user: self)
  end

  # Allow unconfirmed users for now
  # TO DO: remove `confirmed?` once confirmation emails have been set
  def confirmed?
    true
  end

  def regen_algolia_key!
    update!(
      algolia_api_key: generate_algolia_public_key(self)
    )
  end

  private

  def generate_algolia_public_key(user)
    GenerateAlgoliaKey.call(user: user)
  end
end
