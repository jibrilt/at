class Ability
  include CanCan::Ability

  attr_reader :user

  def initialize(user)
    @user = user
    return if user.blank?

    apply_rules
  end

  def apply_rules
    can :manage, User, id: user.id
    can :manage, TrackedItem, user_id: user.id
    can :manage, Tag, user_id: user.id

    can [:create, :read, :update], Message do |target_message|
      target_message.tracked_item.user_id == user.id
    end
  end
end
