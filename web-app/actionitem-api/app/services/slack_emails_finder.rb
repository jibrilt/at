class SlackEmailsFinder < ApplicationService
  attr_accessor :slack_token, :parser

  def initialize(**opts)
    @slack_token = opts[:slack_token] || default_slack_token
    @parser = opts[:parser] || default_parser
  end

  def call
    emails_by_slack_ids(parser.slack_ids)
  end

  private

  def emails_by_slack_ids(slack_ids)
    emails = []
    slack_ids.each do |id|
      emails << slack_user_info(id)[:user][:profile][:email]
    end
    emails
  end

  def slack_user_info(slack_id)
    slack_client.users_info(user: slack_id)
  end

  def slack_client
    SlackClient.call(slack_token: slack_token)
  end

  def default_parser
    missing_attribute(:parser)
  end

  def default_slack_token
    missing_attribute(:slack_token)
  end
end
