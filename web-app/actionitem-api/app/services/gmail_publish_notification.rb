class GmailPublishNotification < ApplicationService
  attr_accessor :notification, :user

  def initialize(notification:, user:)
    @notification = notification || missing_attribute(:notification)
    @user = user || missing_attribute(:user)
  end

  def call
    create_message_history(user, history_id)
    broadcast_message(user)
  end

  private

  def history_id
    notification.history_id
  end

  def create_message_history(user, history_id)
    GmailMessageHistory.create(
      user: user,
      history_id: history_id,
      push_notification: true
    )
  end

  def broadcast_message(user)
    GmailBroadcaster.call(
      previous_history: second_to_last(user),
      user: user
    )
  end

  def second_to_last(user)
    GmailMessageHistory.second_to_the_last(user)
  end
end
