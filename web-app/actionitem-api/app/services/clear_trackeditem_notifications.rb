class ClearTrackeditemNotifications < ApplicationService
  attr_accessor :tracked_item

  def initialize(tracked_item:)
    @tracked_item = tracked_item || missing_attribute(:tracked_item)
  end

  def call
    tracked_item.notifications.unread.each(&:mark_as_read!)
    tracked_item.touch
  end
end
