class GenerateAlgoliaKey < ApplicationService
  attr_accessor :user

  def initialize(user:)
    @user = user || missing_attribute(:user)
  end

  def call
    Algolia.generate_secured_api_key(
      search_key,
      opts
    )
  end

  private

  def opts
    Rails.env.production? ? production_opts : default_opts
  end

  def production_opts
    {
      filters: filters,
      restrictIndices: indices_white_list,
      validUntil: end_of_day
    }
  end

  def default_opts
    {
      filters: filters,
      restrictIndices: indices_white_list
    }
  end

  def filters
    'user_id:' + user.id.to_s
  end

  def indices_white_list
    ['Message']
  end

  def end_of_day
    Time.current.end_of_day.to_i
  end

  def search_key
    ENV['ALGOLIA_SEARCH_KEY']
  end
end
