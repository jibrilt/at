class GmailBroadcaster < ApplicationService
  include GoogleHelpers

  attr_accessor :previous_history, :user

  def initialize(**opts)
    @previous_history = opts[:previous_history]
    @user = opts[:user] || missing_attribute(:user)
  end

  # TO DO: REFACTOR this object

  def call
    return if previous_history.blank?

    history_list = GmailGetHistory.call(
      start_history_id: previous_history.history_id,
      google_credentials: google_credentials
    )

    latest_thread_ids = []
    deleted_thread_ids = []

    history_list.history&.each do |history|
      history.messages_added&.each do |message_added|
        latest_thread_ids << message_added.message.thread_id
      end

      history.messages_deleted&.each do |message_removed|
        deleted_thread_ids << message_removed.message.thread_id
      end
    end

    touched_thread_ids = latest_thread_ids + deleted_thread_ids
    touched_thread_ids.uniq!

    tracked_items = TrackedItem.gmail.where(provider_thread_id: touched_thread_ids, user: user)

    return unless tracked_items.any?

    ActionitemApiSchema.subscriptions.trigger('trackedItemsRefresh', {}, tracked_items)
  end

  private

  def google_credentials
    GmailCredentialsFactory.build(user)
  end
end
