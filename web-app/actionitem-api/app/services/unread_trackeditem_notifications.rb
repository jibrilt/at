class UnreadTrackeditemNotifications < ApplicationService
  include ActionView::Helpers::TextHelper

  attr_accessor :tracked_item

  def initialize(**opts)
    @tracked_item = opts[:tracked_item] || missing_attribute(:tracked_item)
  end

  def call
    text = ''
    notifications.unread_count.each_with_index do |arr, index|
      text << pluralize_unread_notification(arr, index)
    end
    text
  end

  private

  def pluralize_unread_notification(array, index)
    if index + 1 == count
      "#{pluralize(array[1], array[0])}"
    else
      "#{pluralize(array[1], array[0])}, "
    end
  end

  def count
    notifications.unread_count.count
  end

  def notifications
    tracked_item.tracked_item_notifications
  end
end
