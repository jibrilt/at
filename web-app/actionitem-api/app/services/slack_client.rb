class SlackClient < SlackWrapper
  def self.call(**opts)
    new(**opts).process
  end

  def process
    client
  end
end
