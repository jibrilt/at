class DestroyTrackedItem < ApplicationService
  attr_accessor :tracked_item, :user

  def initialize(tracked_item:, user: nil)
    @tracked_item = tracked_item || missing_attribute(:tracked_item)
    @user = user || tracked_item.user || missing_attribute(:user)
  end

  def call
    broadcast_destruction(tracked_item)
    tracked_item.destroy!
  end

  private

  def broadcast_destruction(trackeditem)
    BroadcastTrackedItemList.call(
      tracked_item: trackeditem,
      user: user,
      event_type: 'deleted trackeditem'
    )
  end
end
