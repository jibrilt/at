class RefreshProviderWatches < ApplicationService
  include GoogleHelpers

  attr_accessor :user

  def initialize(**opts)
    @user = opts[:user] || missing_attribute(:user)
  end

  def call
    if user.gmail_provider.present?
      GmailWatchRequest.call(google_credentials: google_credentials)
    end

    # if user.slack_provider.present?
    # end

    # if user.github_provider.present?
    # end
  end

  private

  def google_credentials
    GmailCredentialsFactory.build(user)
  end
end
