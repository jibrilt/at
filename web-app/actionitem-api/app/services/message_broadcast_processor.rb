class MessageBroadcastProcessor < ApplicationService
  attr_accessor :tracked_item, :messages, :user

  def initialize(**opts)
    @tracked_item = opts[:tracked_item] || missing_attribute(:tracked_item)
    @messages = opts[:messages] || missing_attribute(:messages)
    @user = opts[:user] || missing_attribute(:user)
  end

  def call
    return if Rails.env.test?

    if messages.count == 1
      BroadcastTrackedItemList.call(
        tracked_item: tracked_item,
        user: tracked_item.user,
        event_type: 'new trackeditem'
      )
    else
      if tracked_item.email_provider?
        tracked_item.notifications.create(
          notification_type: 'email'
        )
      elsif tracked_item.messaging_provider?
        tracked_item.notifications.create(
          notification_type: 'message'
        )
      elsif tracked_item.posting_provider?
        tracked_item.notifications.create(
          notification_type: 'post'
        )
      end

      BroadcastTrackedItemList.call(
        tracked_item: tracked_item,
        user: user,
        event_type: 'trackeditem new notification'
      )
    end
  end
end
