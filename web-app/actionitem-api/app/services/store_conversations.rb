class StoreConversations < ApplicationService
  include GoogleHelpers

  attr_accessor :tracked_item

  def initialize(**opts)
    @tracked_item = opts[:tracked_item] || missing_attribute(:tracked_item)
  end

  def call
    if tracked_item.gmail?
      gmail_messages = get_gmail_messages(tracked_item)

      gmail_messages.each do |gmail_message|
        save_gmail_message(gmail_message, tracked_item)
      end
    end
    # elsif tracked_item.main_website?
    # elsif tracked_item.slack?
    # elsif tracked_item.asana?
    # end
  end

  private

  def get_gmail_messages(tracked_item)
    GmailMessagesProcessor.call(tracked_item: tracked_item)
  end

  def save_gmail_message(gmail_message, tracked_item)
    ActionItemApi::GmailMessageFactory.build(gmail_message, tracked_item)
  end
end
