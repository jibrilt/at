class SourceDetector < ApplicationService
  attr_accessor :request

  def initialize(**opts)
    @request = opts[:request] || default_request
  end

  def call
    case source_host
    when 'https://mail.google.com'
      'gmail'
    else
      'main_website'
    end
  end

  private

  def source_host
    request.headers['HTTP_ORIGIN']
  end

  def default_request
    missing_attribute(:request)
  end
end
