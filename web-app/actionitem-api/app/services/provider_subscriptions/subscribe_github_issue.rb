class SubscribeGithubIssue < ApplicationService
  attr_accessor :issue_id

  def initialize(**opts)
    @issue_id = opts[:issue_id] || missing_attribute(:issue_id)
  end

  def call
  end

  private

  def webhook_secret
    ENV['GITHUB_WEBHOOK_SECRET']
  end
end
