class BroadcastTrackedItemList < ApplicationService
  attr_accessor :tracked_item, :user, :event_type

  def initialize(tracked_item:, user:, event_type:)
    @tracked_item = tracked_item || missing_attribute(:tracked_item)
    @user = user || missing_attribute(:user)
    @event_type = event_type || missing_attribute(:event_type)

    validate_event_type(@event_type)
  end

  def call
    return if Rails.env.test?

    trigger_subscription(
      tracked_item.gql_id,
      user.id,
      event_type
    )
  end

  private

  def trigger_subscription(gql_id, user_id, event_type)
    ActionitemApiSchema.subscriptions.trigger(
      subscription_name,
      {},
      {
        tracked_item_gql_id: gql_id,
        event_type: event_type,
        tracked_item: [ tracked_item ]
      },
      scope: user_id
    )
  end

  def subscription_name
    'trackedItemUpdated'
  end

  def validate_event_type(event_type)
    return if event_type_whitelist.member?(event_type)

    raise validation_message
  end

  def validation_message
    'event_type is not in the whitelist'
  end

  def event_type_whitelist
    [
      'new trackeditem',
      'trackeditem update',
      'trackeditem clear notifications',
      'trackeditem new notification',
      'deleted message',
      'deleted trackeditem'
    ]
  end
end
