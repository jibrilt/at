class GmailMessagesProcessor < ApplicationService
  include GoogleHelpers

  attr_accessor :tracked_item, :user

  def initialize(**opts)
    @tracked_item = opts[:tracked_item] || default_tracked_item
    @user = opts[:user] || default_user
  end

  def call
    return [] if gmail_access_token.blank?

    thread_id = tracked_item.provider_thread_id
    thread = get_gmail_thread(thread_id)
    build_messages(thread)
  end

  private

  def get_gmail_thread(thread_id)
    GmailGetThread.call(
      google_credentials: google_credentials,
      thread_id: thread_id
    )
  end

  def build_messages(thread)
    GmailMessagesFactory.build(thread.messages)
  end

  def gmail_access_token
    google_credentials.access_token
  end

  def google_credentials
    GmailCredentialsFactory.build(user)
  end

  def default_user
    tracked_item.user
  end

  def default_tracked_item
    missing_attribute(:tracked_item)
  end
end
