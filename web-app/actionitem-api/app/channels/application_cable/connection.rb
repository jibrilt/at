module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = current_user
    end

    private

    def current_user
      token = auth_token
      user = user_by_token(token)
      return reject_unauthorized_connection if user.blank?

      user
    end

    def auth_token
      request.params[:token]
    end

    def user_by_token(token)
      User.find_for_database_authentication(authentication_token: token)
    end
  end
end
