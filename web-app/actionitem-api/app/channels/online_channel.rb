class OnlineChannel < ApplicationCable::Channel
  def subscribed
    redis_key = "user:#{current_user.id}"

    if redis_connection.exists?(redis_key)
      redis_connection.hincrby(
        redis_key,
        'connections', '1'
      )
    else
      redis_connection.hset(
        redis_key,
        'connections', '1'
      )
    end

    return unless user_connections_count(redis_key) == '1'
  end

  def unsubscribed
    redis_key = "user:#{current_user.id}"

    return if user_connections_count(redis_key) == '0'

    redis_connection.hincrby(
      redis_key,
      'connections', '-1'
    )

    return unless user_connections_count(redis_key) == '0'
  end

  private

  def redis_connection
    ActionCable.server.pubsub.redis_connection_for_subscriptions
  end

  def user_connections_count(redis_key)
    redis_connection.hmget(redis_key, 'connections')[0]
  end
end
