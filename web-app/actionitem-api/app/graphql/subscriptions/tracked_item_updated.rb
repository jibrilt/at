module Subscriptions
  class TrackedItemUpdated < Subscriptions::BaseSubscription
    description 'sends the tracked items gql_id after an update'

    subscription_scope :current_user_id

    field :tracked_item_gql_id, String, null: true
    field :tracked_item, Types::TrackedItemType.connection_type, null: true
    field :event_type, String, null: false

    def subscribe; end
  end
end
