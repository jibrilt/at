module Types
  class TagType < Types::BaseObject
    implements GraphQL::Types::Relay::Node

    graphql_name 'Tag'

    guard ->(object, _args, context) {
      context[:ability].can? :read, object.object
    }

    global_id_field :id

    field :name, String, null: false
    field :tracked_item, Types::TrackedItemType, null: false

    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
