module Types
  class SubscriptionType < Types::BaseObject
    field :tracked_item_updated,
      subscription: Subscriptions::TrackedItemUpdated,
      null: true
  end
end
