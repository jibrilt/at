module Types
  class QueryType < Types::BaseObject
    field :current_user, Types::UserType, null: true
    field :messages, [Types::MessageType], null: true
    field :tracked_items, [Types::TrackedItemType], null: true

    field :tracked_items_connection,
      Types::TrackedItemType.connection_type,
      null: false

    field :tracked_item_by_message, Types::TrackedItemType, null: true do
      argument :provider_thread_id, String, required: true
    end

    def tracked_items_connection
      tracked_items_by_latest_message
    end

    def current_user
      context[:current_user]
    end

    def tracked_items
      tracked_items_by_latest_message
    end

    def tracked_item_by_message(provider_thread_id:)
      TrackedItem.find_by(
        provider_thread_id: provider_thread_id
      )
    end

    private

    def tracked_items_by_latest_message
      context[:current_user]
        .tracked_items
        .by_latest_message
    end
  end
end
