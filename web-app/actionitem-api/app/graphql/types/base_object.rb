module Types
  class BaseObject < GraphQL::Schema::Object
    include GoogleHelpers

    field_class Types::BaseField
    add_field(GraphQL::Types::Relay::NodeField)
    add_field(GraphQL::Types::Relay::NodesField)
  end
end
