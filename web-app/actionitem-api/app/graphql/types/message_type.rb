module Types
  class MessageType < Types::BaseObject
    graphql_name 'Message'

    global_id_field :id

    field :tracked_item, Types::TrackedItemType, null: false

    field :provider_message_id, String, null: false
    field :body, String, null: false
    field :deep_link, String, null: true

    field :provider_date_posted, GraphQL::Types::ISO8601DateTime, null: true
    field :provider_date_posted_i, Integer, null: false

    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
