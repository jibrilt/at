module Types
  class MutationType < Types::BaseObject
    # User
    field :register_user, mutation: Mutations::Users::RegisterUser
    field :sign_in, mutation: Mutations::Users::SignIn
    field :sign_out, mutation: Mutations::Users::SignOut
    field :destroy_user, mutation: Mutations::Users::Destroy

    # Tracked Item
    field :create_tracked_item, mutation: Mutations::TrackedItems::Create
    field :update_tracked_item, mutation: Mutations::TrackedItems::Update
    field :destroy_tracked_item, mutation: Mutations::TrackedItems::Destroy

    # Gmail Tracking
    field :stop_gmail_tracking, mutation: Mutations::StopGmailTracking

    # Clear TrackedItem Notifications
    field :clear_ti_notifications, mutation: Mutations::TrackedItemNotifications::Clear

    # Tags
    field :create_tag, mutation: Mutations::Tags::Create
    field :destroy_tag, mutation: Mutations::Tags::Destroy

    # Oauth
    field :github_oauth, mutation: Mutations::Oauth::Github
  end
end
