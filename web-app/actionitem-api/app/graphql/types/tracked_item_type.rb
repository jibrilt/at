module Types
  class TrackedItemType < Types::BaseObject
    implements GraphQL::Types::Relay::Node

    graphql_name 'TrackedItem'

    guard ->(object, _args, context) {
      context[:ability].can? :read, object.object
    }

    global_id_field :id

    field :user, Types::UserType, null: false
    field :provider, String, null: false
    field :provider_thread_id, String, null: true
    field :initial_subject, String, null: false
    field :initial_from, String, null: false
    field :initial_from_email, String, null: false
    field :unread_notifications_to_s, String, null: false

    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false

    field :latest_message, Types::MessageType, null: false

    field :messages, [Types::MessageType], null: true
    field :tags, [Types::TagType], null: true
  end
end
