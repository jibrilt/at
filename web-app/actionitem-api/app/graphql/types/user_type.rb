module Types
  class UserType < Types::BaseObject
    implements GraphQL::Types::Relay::Node

    graphql_name 'User'

    guard ->(object, _args, context) { context[:ability].can? :read, object.object }

    global_id_field :id

    field :email, String, null: false
    field :authentication_token, String, null: false
    field :signed_in_via, String, null: true
    field :algolia_api_key, String, null: false

    def authentication_token
      guard_column(:authentication_token)

      object.authentication_token
    end

    private

    def guard_column(column)
      return unless object.gql_id != context[:current_user]&.gql_id

      raise GraphQL::UnauthorizedFieldError, "Unable to access #{column}"
    end
  end
end
