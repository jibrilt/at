module Types
  class TrackedItemUpdatedType < Types::BaseObject
    graphql_name 'TrackedItemUpdated'

    # Type is only for the subscription

    field :tracked_item_gql_id, String, null: false
  end
end
