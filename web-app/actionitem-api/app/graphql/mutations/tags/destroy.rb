module Mutations
  module Tags
    class Destroy < Mutations::BaseMutation
      graphql_name 'DestroyTag'

      argument :gql_id, ID, required: true

      def resolve(args)
        tag = find_tag(args)
        return graphql_unauthorized unless user_permitted?(tag)

        result(tag.destroy!)
      end

      private

      def find_tag(args)
        Tag.find_by_gql_id(args[:gql_id])
      end

      def result(tag)
        MutationResult.call(
          success: !tag.persisted?,
          errors: tag.errors
        )
      end

      def user_permitted?(tag)
        context[:ability].can? :destroy, tag
      end

      def unauthorized_message
        'User is unauthorized to destroy this tag'
      end
    end
  end
end
