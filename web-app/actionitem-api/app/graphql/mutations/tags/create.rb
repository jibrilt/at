module Mutations
  module Tags
    class Create < Mutations::BaseMutation
      graphql_name 'CreateTag'

      argument :name, String, required: true
      argument :trackeditem_gql_id, String, required: true

      field :tag, Types::TagType, null: false

      def resolve(args)
        tracked_item = find_tracked_item(args[:trackeditem_gql_id])
        return graphql_unauthorized unless user_permitted?(tracked_item)

        tag = create_tag(args, tracked_item)
        result({ tag: tag }, tag)
      rescue ActiveRecord::RecordInvalid => e
        record_invalid(e)
      end

      private

      def create_tag(args, tracked_item)
        Tag.create!(
          name: args[:name],
          tracked_item: tracked_item
        )
      end

      def find_tracked_item(gql_id)
        tracked_item = TrackedItem.find_by_gql_id(gql_id)
        raise ActiveRecord::RecordInvalid unless tracked_item.present?

        tracked_item
      end

      def user_permitted?(tracked_item)
        context[:ability].can?(:create, Tag) &&
        context[:ability].can?(:update, tracked_item)
      end
    end
  end
end
