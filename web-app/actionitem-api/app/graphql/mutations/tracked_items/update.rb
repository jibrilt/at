module Mutations
  module TrackedItems
    class Update < Mutations::BaseMutation
      graphql_name 'UpdateTrackedItemy'

      argument :id, ID, required: true

      field :tracked_item, Types::TrackedItemType, null: false

      def resolve(args)
        tracked_item = find_tracked_item(args)
        return graphql_unauthorized unless user_permitted?(tracked_item)

        update_tracked_item(args, tracked_item)
        result({ tracked_item: tracked_item }, tracked_item)
      rescue ActiveRecord::RecordInvalid => e
        record_invalid(e)
      end

      private

      def find_tracked_item(args)
        TrackedItem.find_by_gql_id(args[:id])
      end

      def update_tracked_item(args, tracked_item)
        tracked_item.update!(args.except(:id))
      end

      def user_permitted?(tracked_item)
        context[:ability].can? :update, tracked_item
      end

      def unauthorized_message
        'User is unauthorized to update a tracked_item'
      end
    end
  end
end
