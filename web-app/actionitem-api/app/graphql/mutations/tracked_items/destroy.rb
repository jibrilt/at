module Mutations
  module TrackedItems
    class Destroy < Mutations::BaseMutation
      graphql_name 'DestroyTrackedItem'

      argument :id, ID, required: true

      def resolve(args)
        tracked_item = find_tracked_item(args)
        return graphql_unauthorized unless user_permitted?(tracked_item)

        result(destroy_tracked_item(tracked_item))
      end

      private

      def destroy_tracked_item(tracked_item)
        DestroyTrackedItem.call(
          tracked_item: tracked_item
        )
      end

      def find_tracked_item(args)
        TrackedItem.find_by_gql_id(args[:id])
      end

      def result(tracked_item)
        MutationResult.call(
          success: !tracked_item.persisted?,
          errors: tracked_item.errors
        )
      end

      def user_permitted?(tracked_item)
        context[:ability].can? :destroy, tracked_item
      end

      def unauthorized_message
        'User is unauthorized to destroy a tracked_item'
      end
    end
  end
end
