module Mutations
  module TrackedItems
    class Create < Mutations::BaseMutation
      graphql_name 'CreateTrackedItem'

      argument :provider_thread_id, String, required: false

      field :tracked_item, Types::TrackedItemType, null: false

      def resolve(args)
        return graphql_unauthorized unless user_permitted?

        tracked_item = create_tracked_item(args)
        save_conversations(tracked_item)
        result({ tracked_item: tracked_item }, tracked_item)
      rescue ActiveRecord::RecordInvalid => e
        record_invalid(e)
      end

      private

      def create_tracked_item(args)
        TrackedItem.create!(args
          .merge(
            user: context[:current_user],
            provider: host_source
          ))
      end

      def host_source
        return if context[:request].blank?

        SourceDetector.call(request: context[:request])
      end

      def save_conversations(tracked_item)
        StoreConversations.call(tracked_item: tracked_item)
      end

      def user_permitted?
        context[:ability].can? :create, TrackedItem
      end

      def unauthorized_message
        'User is unauthorized to create a tracked item'
      end
    end
  end
end
