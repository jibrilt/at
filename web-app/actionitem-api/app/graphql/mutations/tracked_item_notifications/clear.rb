module Mutations
  module TrackedItemNotifications
    class Clear < Mutations::BaseMutation
      graphql_name 'ClearTiNotifications'

      argument :tracked_item_gql_id, ID, required: true

      def resolve(args)
        tracked_item = find_tracked_item(args[:tracked_item_gql_id])
        return graphql_unauthorized unless user_permitted?(tracked_item)

        clear_notifications(tracked_item)
        broadcast_clearing(tracked_item)
        result
      rescue ActiveRecord::RecordInvalid => e
        record_invalid(e)
      end

      private

      def find_tracked_item(gql_id)
        context[:current_user]
          .tracked_items
          .find_by_gql_id(gql_id)
      end

      def clear_notifications(tracked_item)
        ClearTrackeditemNotifications.call(
          tracked_item: tracked_item
        )
      end

      def result
        MutationResult.call(
          success: true,
          errors: nil
        )
      end

      def user_permitted?(tracked_item)
        context[:ability].can? :update, tracked_item
      end

      def broadcast_clearing(tracked_item)
        BroadcastTrackedItemList.call(
          tracked_item: tracked_item,
          user: tracked_item.user,
          event_type: broadcast_event_type
        )
      end

      def broadcast_event_type
        'trackeditem clear notifications'
      end
    end
  end
end
