module Mutations
  module Oauth
    class Github < Mutations::BaseMutation
      graphql_name 'GithubOauth'

      argument :code, String, required: true

      def resolve(args)
        user = context[:current_user]
        return graphql_unauthorized unless user_permitted?(user)

        access_token = github_access_token(args[:code])
        create_github_access_token(user, access_token)
        mutation_result
      rescue ActiveRecord::RecordInvalid => e
        record_invalid(e)
      end

      private

      def github_access_token(code)
        github_auth = OauthHelpers::GithubAuth.new(code: code)
        github_auth.get_access_token
      end

      def create_github_access_token(user, access_token)
        github_provider = create_github_provider(user)
        github_provider.update!(
          access_token: access_token
        )
      end

      def create_github_provider(user)
        user
          .oauth_providers
          .find_or_create_by!(
            provider: 'github'
          )
      end

      def mutation_result
        MutationResult.call(
          success: true,
          errors: []
        )
      end

      def user_permitted?(user)
        context[:ability].can?(:update, user)
      end
    end
  end
end
