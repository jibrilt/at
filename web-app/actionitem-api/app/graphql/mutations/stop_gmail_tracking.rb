module Mutations
  class StopGmailTracking < Mutations::BaseMutation
    graphql_name 'StopGmailTracking'

    def resolve
      return graphql_unauthorized unless user_permitted?(context[:current_user])

      stop_gmail_tracking(context[:current_user])
      success_result
    end

    private

    def stop_gmail_tracking(user)
      GmailStopWatching.call(access_token: user.gmail_access_token)
    end

    def user_permitted?(user)
      user.present?
    end

    def success_result
      MutationResult.call(
        success: true
      )
    end

    def unauthorized_message
      'User is unauthorized to update a tracked_item'
    end
  end
end
