module Mutations
  module Users
    class SignIn < Mutations::BaseMutation
      graphql_name 'SignIn'

      argument :email, String, required: true
      argument :password, String, required: true

      field :user, Types::UserType, null: false

      def resolve(args)
        user = find_user(args)
        if user.present?
          if user.valid_password?(args[:password])
            ability(user)
            regen_algolia_key(user)
            result({ user: user }, user)
          else
            GraphQL::ExecutionError.new(incorrect_combo_msg)
          end
        else
          GraphQL::ExecutionError.new(user_not_registered_msg)
        end
      end

      private

      def find_user(args)
        User.find_for_database_authentication(email: args[:email])
      end

      def ability(user)
        context[:current_user] = user
        context[:ability] = Ability.new(user)
      end

      def regen_algolia_key(user)
        user.update(
          algolia_api_key: generate_algolia_public_key(user)
        )
      end

      def generate_algolia_public_key(user)
        GenerateAlgoliaKey.call(user: user)
      end

      def incorrect_combo_msg
        'Incorrect Email/Password'
      end

      def user_not_registered_msg
        'User not registered on this application'
      end
    end
  end
end
