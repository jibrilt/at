module Mutations
  module Users
    class Destroy < Mutations::BaseMutation
      graphql_name 'DestroyUser'

      argument :uid, String, required: true

      def resolve(args)
        user = find_user(args)
        if user.present?
          user.destroy!
          context[:current_user] = nil
        end
        result(user)
      rescue ActiveRecord::RecordInvalid => e
        record_invalid(e)
      end

      private

      def result(user)
        MutationResult.call(
          success: !user.persisted?,
          errors: user.errors
        )
      end

      def find_user(args)
        User.find_by_gql_id(args[:uid])
      end
    end
  end
end
