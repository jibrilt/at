module Mutations
  module Users
    class SignOut < Mutations::BaseMutation
      graphql_name 'SignOut'

      field :user, Types::UserType, null: false

      def resolve
        user = context[:current_user]
        return GraphQL::ExecutionError.new(execution_error_msg) if user.blank?

        result(user)
      end

      private

      def execution_error_msg
        'User not signed in'
      end

      def result(user)
        MutationResult.call(
          obj: { user: user },
          success: true,
          errors: user.errors
        )
      end
    end
  end
end
