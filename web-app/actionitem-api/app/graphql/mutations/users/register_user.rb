module Mutations
  module Users
    class RegisterUser < Mutations::BaseMutation
      graphql_name 'RegisterUser'

      argument :email, String, required: true
      argument :password, String, required: true

      field :user, Types::UserType, null: false

      def resolve(args)
        user = User.create!(args)

        context[:current_user] = user
        context[:ability] = Ability.new(user)
        result({ user: user }, user)
      rescue ActiveRecord::RecordInvalid => e
        record_invalid(e)
      end
    end
  end
end
