module Mutations
  class BaseMutation < GraphQL::Schema::RelayClassicMutation
    include GoogleHelpers

    argument_class Types::BaseArgument
    field_class Types::BaseField
    input_object_class Types::BaseInputObject
    object_class Types::BaseObject

    field :success, Boolean, null: false
    field :errors, [String], null: true

    protected

    def authorize_user
      return true if context[:current_user].present?

      raise GraphQL::ExecutionError, 'User not signed in'
    end

    def graphql_unauthorized
      raise GraphQL::Guard::NotAuthorizedError.new(unauthorized_message)
    end

    def result(obj_wrap, obj)
      MutationResult.call(
        obj: obj_wrap,
        success: obj.persisted?,
        errors: obj.errors
      )
    end

    private

    def unauthorized_message
      'User is unauthorized'
    end

    def record_invalid(error)
      GraphQL::ExecutionError.new(
        "Invalid Attributes for #{error.record.class.name}: " \
        "#{error.record.errors.full_messages.join(', ')}"
      )
    end
  end
end
