require 'spec_helper'

RSpec.describe Message do
  let(:message) { build(:message) }

  subject { message }

  it { is_expected.to be_valid }

  describe 'associations' do
    it {
      is_expected.to belong_to(:tracked_item)
        .class_name('TrackedItem')
        .required
    }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:tracked_item_id) }
    it { is_expected.to validate_presence_of(:provider_message_id) }
    it { is_expected.to validate_presence_of(:deep_link) }
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:provider_date_posted) }
  end

  describe 'public interface' do
  end
end
