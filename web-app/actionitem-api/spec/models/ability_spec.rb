require 'spec_helper'
require 'cancan/matchers'

RSpec.describe Ability do
  subject(:ability) { described_class.new(user) }

  let(:user) { create(:user) }

  describe 'Tracked Item' do
    let(:tracked_item) { create(:tracked_item) }

    context 'created by the user' do
      let(:user) { tracked_item.user }

      it { is_expected.to be_able_to(:manage, tracked_item) }
    end

    context 'created by a different user' do
      it { should_not be_able_to(:manage, tracked_item) }
    end
  end

  describe 'Message' do
    let(:message) { create(:message) }

    context 'created by the user' do
      let(:user) { message.tracked_item.user }

      it { should_not be_able_to(:manage, message) }

      it { should_not be_able_to(:destroy, message) }

      it { is_expected.to be_able_to(:create, message) }
      it { is_expected.to be_able_to(:update, message) }
      it { is_expected.to be_able_to(:read, message) }
    end

    context 'created by a different user' do
      it { should_not be_able_to(:create, message) }
      it { should_not be_able_to(:update, message) }
      it { should_not be_able_to(:read, message) }
    end
  end

  describe 'User' do
    it { is_expected.to be_able_to(:manage, user) }

    context 'other users' do
      let(:another_user) { create(:user) }

      it { should_not be_able_to(:manage, another_user) }
    end
  end

  describe 'Tags' do
    let(:tag) { create(:tag) }

    context 'created by the user' do
      let(:user) { tag.user }

      it { should be_able_to(:manage, tag) }
    end

    context 'created by a different user' do
      it { should_not be_able_to(:create, tag) }
    end
  end
end
