require 'spec_helper'

RSpec.describe TrackedItem do
  let(:tracked_item) { build(:tracked_item) }

  subject { tracked_item }

  it { is_expected.to be_valid }

  describe 'associations' do
    it {
      is_expected.to belong_to(:user)
        .class_name('User')
        .required
    }

    it {
      should have_many(:messages)
        .class_name('Message')
        .dependent(:destroy)
    }

    it {
      should have_many(:tracked_item_notifications)
        .class_name('TrackedItemNotification')
        .dependent(:destroy)
    }

    it {
      should have_many(:tags)
        .class_name('Tag')
        .dependent(:destroy)
    }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.to validate_presence_of(:provider) }
    it { is_expected.to validate_presence_of(:provider_thread_id) }
    it { is_expected.to validate_presence_of(:initial_from_email) }
    it { is_expected.to validate_presence_of(:initial_from) }
    it { is_expected.to validate_presence_of(:initial_subject)}
  end

  describe 'callbacks' do
  end

  describe 'public interface' do
  end
end
