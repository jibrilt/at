require 'spec_helper'

RSpec.describe Tag do
  let(:tag) { build(:tag) }

  subject { tag }

  it { is_expected.to be_valid }

  describe 'associations' do
    it { should belong_to(:tracked_item).required }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:tracked_item_id) }
  end

  describe 'callbacks' do
    it 'automatically sets the user id' do
      expect(tag.user).to be_nil
      tag.save
      expect(tag.user).to eq(tag.tracked_item.user)
    end

    it 'downcases the name' do
      name = Faker::Alphanumeric.alphanumeric(number: 15).upcase
      tag.name = name
      tag.save
      expect(tag.name).to eq(name.downcase)
    end
  end
end
