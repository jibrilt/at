require 'spec_helper'

RSpec.describe TrackedItemNotification do
  let(:tracked_item_notification) { build(:tracked_item_notification) }

  subject { tracked_item_notification }

  it { is_expected.to be_valid }

  describe 'associations' do
    it {
      should belong_to(:tracked_item)
        .class_name('TrackedItem')
    }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:tracked_item_id) }
    it { is_expected.to validate_presence_of(:notification_type) }
    it { is_expected.to validate_presence_of(:aasm_state) }


    describe 'notification_type' do
      it { is_expected.to validate_presence_of(:notification_type) }

      described_class::NOTIFICATION_TYPES.each do |plan|
        it {
          is_expected.to allow_value(plan).for(:notification_type)
        }
      end

      it {
        is_expected.to_not(
          allow_value(
            Faker::Alphanumeric.alphanumeric(number: 15)
          ).for(:notification_type)
        )
      }
    end
  end

  describe 'callbacks' do
  end
end
