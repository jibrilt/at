require 'spec_helper'

RSpec.describe User do

  let(:user) { build(:user) }

  subject { user }

  it { is_expected.to be_valid }

  describe 'associations' do
    it {
      should have_many(:tracked_items)
        .class_name('TrackedItem')
        .dependent(:destroy)
    }

    it {
      should have_many(:tags)
        .class_name('Tag')
    }
  end

  describe 'callbacks' do
    describe 'after create' do
      describe 'regen_algolia_key' do
        it 'generates an api key' do
          expect(user.algolia_api_key).to be_blank
          user.save
          expect(user.algolia_api_key).to be_present
        end
      end
    end
  end
end
