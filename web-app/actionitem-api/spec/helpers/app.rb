def t(translation, **args)
  I18n.t(translation, **args)
end

def app_temp_key
  Base64.urlsafe_decode64(ENV['APP_TEMP_KEY'])
end

def slack_token_key
  Base64.urlsafe_decode64(ENV['SLACK_TOKEN_KEY'])
end

def app_default_port
  43565
end

def provider_whitelist
  ProviderMarkable::PROVIDER_WHITELIST
end

# Local Host Stuff

def app_default_host_name
  'app.localhost'
end

def app_default_host
  "http://#{app_default_host_name}"
end

def set_default_host
  Rails.application.routes.default_url_options[:host] = "#{app_default_host}:#{app_default_port}"
end
