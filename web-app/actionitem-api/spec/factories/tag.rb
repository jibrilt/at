FactoryBot.define do
  factory :tag do
    tracked_item { create(:tracked_item) }
    name { Faker::Alphanumeric.alphanumeric(number: 10) }
  end
end
