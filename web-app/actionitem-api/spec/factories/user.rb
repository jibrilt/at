FactoryBot.define do
  factory :user do
    sequence :email do |n|
      "#{n}#{Faker::Internet.email}"
    end
    password { Faker::Alphanumeric.alphanumeric(number: 10) }
  end
end
