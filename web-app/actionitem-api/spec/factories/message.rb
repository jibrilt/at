FactoryBot.define do
  factory :message do
    tracked_item { create(:tracked_item) }

    deep_link { Faker::Internet.url }
    body { Faker::Alphanumeric.alphanumeric(number: 15) }
    provider_date_posted { rand(3.days).seconds.ago }
    provider_message_id { Faker::Alphanumeric.alphanumeric(number: 15) }
  end
end
