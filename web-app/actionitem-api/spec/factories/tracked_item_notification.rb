FactoryBot.define do
  factory :tracked_item_notification do
    tracked_item { create(:tracked_item) }

    notification_type { TrackedItemNotification::NOTIFICATION_TYPES.sample }
    provider_abstract_id { Faker::Alphanumeric.alphanumeric(number: 15) }

    trait :read do
      after(:create) do |notification, evaluator|
        notification.mark_as_read!
      end
    end
  end
end
