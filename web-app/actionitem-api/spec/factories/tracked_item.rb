FactoryBot.define do
  factory :tracked_item do
    user { create(:user) }
    provider { ProviderMarkable::PROVIDER_WHITELIST.sample }
    provider_thread_id { Faker::Alphanumeric.alphanumeric(number: 15) }
    initial_from { Faker::Alphanumeric.alphanumeric(number: 15) }
    initial_subject { Faker::Alphanumeric.alphanumeric(number: 15) }

    sequence :initial_from_email do |n|
      "#{n}#{Faker::Internet.email}"
    end

    after(:create) do |tracked_item, evaluator|
      create_list(:message, 1, tracked_item: tracked_item)

      tracked_item.reload
    end
  end
end
