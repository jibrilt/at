require 'spec_helper'

shared_examples_for 'provider_markable' do
  let!(:model) { create(described_class.to_s.underscore) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:provider) }
    it { is_expected.to validate_inclusion_of(:provider).in_array(provider_whitelist) }
  end
end
