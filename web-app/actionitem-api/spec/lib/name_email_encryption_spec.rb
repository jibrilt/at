require 'spec_helper'

RSpec.describe NameEmailEncryption do
  let(:token) { SecureRandom.random_bytes(32) }
  let(:encoded_token) { Base64.strict_encode64(token) }

  describe 'attributes' do
    subject { described_class.new }

    it { is_expected.to respond_to(:state) }
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:email) }
  end

  describe 'public interface' do
    it { is_expected.to respond_to(:decrypt) }
    it { is_expected.to respond_to(:encrypt) }

    describe 'encode' do
      subject { described_class.new.encode(token) }

      it { is_expected.to eq(encoded_token) }
    end

    describe 'decode' do
      subject { described_class.new.decode(encoded_token) }

      it { is_expected.to eq(token) }
    end

    describe 'crypt' do
      subject { described_class.new.crypt }

      it { is_expected.to be_instance_of(ActiveSupport::MessageEncryptor) }
    end
  end
end
