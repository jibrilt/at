require 'spec_helper'

RSpec.describe NameEmailEncryptor do
  let(:name) { Faker::Alphanumeric.alpha(number: 10) }
  let(:email) { Faker::Internet.email }

  describe 'inheritance' do
    subject { described_class }

    it { is_expected.to be < NameEmailEncryption }
  end

  describe 'validations' do
    context 'missing name' do
      it 'raises an error' do
        expect { described_class.new(name: nil, email: email) }
          .to raise_error(NotImplementedError, I18n.t('lib.missing_attribute', attr: :name))
      end
    end

    context 'missing user email' do
      it 'raises an error' do
        expect { described_class.new(email: nil, name: name) }
          .to raise_error(NotImplementedError, I18n.t('lib.missing_attribute', attr: :email))
      end
    end
  end

  describe 'attributes' do
    subject do
      described_class.new(name: name, email: email)
    end

    it {
      is_expected.to have_attributes(
        name: name,
        email: email,
        state: "#{name} #{email}"
      )
    }
  end

  describe 'public interface' do
    subject do
      described_class.new(name: name, email: email)
    end

    it { is_expected.to respond_to(:encrypt_state) }
    it { is_expected.to respond_to(:encode_state) }
  end
end
