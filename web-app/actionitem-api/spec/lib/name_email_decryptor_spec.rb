require 'spec_helper'

RSpec.describe NameEmailDecryptor do
  let(:name) { Faker::Alphanumeric.alpha(number: 10) }
  let(:email) { Faker::Internet.email }
  let(:state_format) { "#{name} #{email}" }

  let(:state_hash) do
    token_hash = encryptor.encrypt_and_sign(state_format, purpose: :user_verification)
    Base64.urlsafe_encode64(token_hash)
  end

  describe 'inheritance' do
    subject { described_class }

    it { is_expected.to be < NameEmailEncryption }
  end

  describe 'validations' do
    context 'missing state_hash' do
      it 'raises an error' do
        expect { described_class.new(state_hash: nil) }
          .to raise_error(NotImplementedError, I18n.t('lib.missing_attribute', attr: :state_hash))
      end
    end
  end

  describe 'attributes' do
    subject { described_class.new(state_hash: state_hash) }

    it {
      is_expected.to have_attributes(
        name: name,
        email: email,
        state: state_format
      )
    }
  end

  describe 'public interface' do
    let(:state_decryptor) { described_class.new(state_hash: state_hash) }
    let(:decoded_hash) { Base64.urlsafe_decode64(state_hash) }

    describe 'decoded_state_hash' do
      subject { state_decryptor.decoded_state_hash }

      it { is_expected.to eq(decoded_hash) }
    end

    describe 'decrypted_state_hash' do
      subject { state_decryptor.decrypted_state_hash }

      it { is_expected.to eq(state_format) }
    end
  end

  private

  def encryptor
    ActiveSupport::MessageEncryptor.new(app_temp_key)
  end
end
