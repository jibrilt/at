class SlackChannelSender < SlackWrapper
  attr_accessor :blocks, :channel

  def post_initialize(**opts)
    @blocks = opts[:blocks] || default_blocks
    @channel = opts[:channel] || default_channel
  end

  def self.send_with(**opts)
    new(**opts).send_by_blocks
  end

  def send_by_blocks
    client.chat_postMessage(
      channel: channel,
      blocks: blocks
    )
  end

  private

  def default_blocks
    raise NotImplementedError, t('lib.missing_attribute', attr: :blocks)
  end

  def default_channel
    raise NotImplementedError, t('lib.missing_attribute', attr: :channel)
  end

  def default_slack_token
    raise NotImplementedError, t('lib.missing_attribute', attr: :slack_token)
  end
end
