require 'spec_helper'

RSpec.describe Types::UserType do
  it 'returns the user if current user is the same' do
    user = create(:user)
    variables = { 'id' => user.gql_id }

    result = gql_query(
      query: query,
      variables: variables,
      context: {
        current_user: user,
        ability: Ability.new(user)
      }
    ).to_h.deep_symbolize_keys

    expect(result.dig(:data, :node, :id)).to eq(user.gql_id)
    expect(result.dig(:data, :node, :email)).to eq(user.email)
    expect(result.dig(:data, :node, :authenticationToken)).
      to eq(user.authentication_token)

    expect(
      result.dig(:data, :node, :algoliaApiKey)
    ).to eq(user.algolia_api_key)

    expect(result[:errors]).to be_blank
  end

  it 'will raise an error if not logged in' do
    user = create(:user)
    variables = { 'id' => user.gql_id }

    expect {
      gql_query(
        query: query,
        variables: variables,
        context: {
          current_user: nil,
          ability: Ability.new(nil)
        }
      )
    }.to raise_error GraphQL::Guard::NotAuthorizedError
  end

  it 'will raise an error for non current user' do
    user = create(:user)
    evil_user = create(:user)

    variables = { 'id' => user.gql_id }

    expect {
      gql_query(
        query: query,
        variables: variables,
        context: {
          current_user: evil_user,
          ability: Ability.new(evil_user)
        }
      )
    }.to raise_error GraphQL::Guard::NotAuthorizedError
  end

  def query
    <<~GQL
      query abstract($id: ID!) {
        node(id: $id) {
          ... on User {
            id
            email
            authenticationToken
            algoliaApiKey
          }
        }
      }
    GQL
  end
end
