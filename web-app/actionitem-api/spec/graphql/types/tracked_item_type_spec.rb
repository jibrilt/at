require 'spec_helper'

RSpec.describe Types::TrackedItemType do
  let(:tracked_item) { create(:tracked_item) }
  let(:user) { tracked_item.user }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  it 'returns the correct parameters' do
    variables = { 'id' => tracked_item.gql_id }

    result = gql_query(
      query: query,
      variables: variables,
      context: {
        current_user: user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(
      result.dig(:data, :node, :id)
    ).to eq(tracked_item.gql_id)

    expect(
      result.dig(:data, :node, :provider)
    ).to eq(tracked_item.provider)

    expect(
      result.dig(:data, :node, :providerThreadId)
    ).to eq(tracked_item.provider_thread_id)

    expect(
      result.dig(:data, :node, :initialSubject)
    ).to eq(tracked_item.initial_subject)

    expect(
      result.dig(:data, :node, :initialFrom)
    ).to eq(tracked_item.initial_from)

    expect(
      result.dig(:data, :node, :initialFromEmail)
    ).to eq(tracked_item.initial_from_email)

    expect(
      result.dig(:data, :node, :initialSubject)
    ).to eq(tracked_item.initial_subject)

    expect(
      result.dig(:data, :node, :unreadNotificationsToS)
    ).to eq('')

    expect(
      result.dig(:data, :node, :user, :id)
    ).to eq(tracked_item.user.gql_id)

    expect(
      result.dig(:data, :node, :user, :email)
    ).to eq(tracked_item.user.email)

    expect(
      result.dig(:data, :node, :createdAt)
    ).to eq(tracked_item.created_at.iso8601)

    expect(
      result.dig(:data, :node, :updatedAt)
    ).to eq(tracked_item.updated_at.iso8601)

    expect(result[:errors]).to be_blank
  end

  it 'raises an authentication error if user is not permitted' do
    variables = { 'id' => tracked_item.gql_id }

    expect {
      gql_query(
        query: query,
        variables: variables,
        context: {
          current_user: user,
          ability: ability_stub_false
        }
      )
    }.to raise_error GraphQL::Guard::NotAuthorizedError
  end

  def query
    <<~GQL
      query abstract($id: ID!) {
        node(id: $id) {
          ... on TrackedItem {
            id
            user {
              id
              email
            }
            createdAt
            updatedAt
            provider
            providerThreadId
            initialSubject
            initialFrom
            initialFromEmail
            unreadNotificationsToS
          }
        }
      }
    GQL
  end
end
