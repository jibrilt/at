require 'spec_helper'

RSpec.describe Types::TagType do
  let(:tag) { create(:tag) }
  let(:tracked_item) { tag.tracked_item }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  xit 'returns the correct parameters' do
    variables = { 'id' => tag.gql_id }

    result = gql_query(
      query: query,
      variables: variables,
      context: {
        current_user: tracked_item.user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(result.dig(:data, :node, :id)).to eq(tag.gql_id)
    expect(result.dig(:data, :node, :name)).to eq(tag.name)
    expect(
      result
      .dig(:data, :node, :tracked_item, :id))
      .to eq(tracked_item.id)

    expect(
      result.dig(:data, :node, :createdAt)
    ).to eq(tag.created_at.iso8601)

    expect(
      result.dig(:data, :node, :updatedAt)
    ).to eq(tag.updated_at.iso8601)
  end

  def query
    <<~GQL
      query abstract($id: ID!) {
        node(id: $id) {
          ... on Tag {
            name
          }
        }
      }
    GQL
  end
end
