require 'spec_helper'

RSpec.describe 'QueryType - tracked_items' do
  let(:user) { create(:user) }
  let(:tracked_items) { create_list(:tracked_item, 5, user: user) }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  it 'returns a list of tracked_items if logged-in' do
    tracked_items
    create_list(:tracked_item, 3)

    result = gql_query(
      query: query,
      context: {
        current_user: user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    result_gql_ids = result.dig(:data, :trackedItems).map { |g| g[:id] }

    expect(tracked_items.map(&:gql_id)).to match_array(result_gql_ids)

    expect(result.dig(:data, :trackedItems).length).to eq(5)
  end

  it 'raises an error if not logged-in' do
    tracked_items
    create_list(:tracked_item, 3)

    expect {
      gql_query(
        query: query,
        context: {
          current_user: nil,
          ability: ability_stub_true
        }
      ).to_h.deep_symbolize_keys
    }.to raise_error NoMethodError
  end

  it 'returns an empty array if no tracked_items' do
    result = gql_query(
      query: query,
      context: {
        current_user: user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(result.dig(:data, :trackedItems)).to eq([])
  end

  def query
    <<~GQL
      query {
        trackedItems {
          id
          provider
          initialSubject
          initialFrom
          initialFromEmail
          unreadNotificationsToS

          latestMessage {
            providerDatePosted
            body
            deepLink
          }
        }
      }
    GQL
  end
end
