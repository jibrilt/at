require 'spec_helper'

RSpec.describe 'QueryType - tracked_item_by_message' do
  let(:provider_thread_id) { Faker::Alphanumeric.alphanumeric(number: 10) }

  let(:user) { create(:user) }

  let(:tracked_item) { create(:tracked_item, provider_thread_id: provider_thread_id) }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  before do
    tracked_item
  end

  it 'returns the tracked_item' do
    variables = {
      'providerThreadId' => provider_thread_id
    }

    result = gql_query(
      query: query,
      variables: variables,
      context: {
        current_user: user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(
      result.dig(:data, :trackedItemByMessage, :providerThreadId)
    ).to eq(variables['providerThreadId'])
  end

  it 'returns nil if no tracked_item' do
    variables = {
      'providerThreadId' => Faker::Alphanumeric.alphanumeric(number: 10)
    }

    result = gql_query(
      query: query,
      variables: variables,
      context: {
        current_user: user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(result.dig(:data, :trackedItemByMessage)).to be_nil
  end

  def query
    <<~GQL
      query abstract($providerThreadId: String!){
        trackedItemByMessage(providerThreadId: $providerThreadId) {
          id
          providerThreadId
        }
      }
    GQL
  end
end
