require 'spec_helper'

RSpec.describe 'QueryType - current_user' do
  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  it 'returns the current logged in user' do
    users = create_list(:user, 3)
    user = users.first

    result = gql_query(
      query: query, context: {
        current_user: users.first,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(result.dig(:data, :currentUser, :id)).to eq(user.gql_id)
    expect(result.dig(:data, :currentUser, :email)).to eq(user.email)
    expect(result.dig(:data, :currentUser, :authenticationToken)).
      to eq(user.authentication_token)

    third_user = users.third
    result = gql_query(
      query: query, context: {
        current_user: users.third,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(result.dig(:data, :currentUser, :id)).to eq(third_user.gql_id)
    expect(result.dig(:data, :currentUser, :email)).to eq(third_user.email)
    expect(result.dig(:data, :currentUser, :authenticationToken)).
      to eq(third_user.authentication_token)
  end

  it 'returns nil if not logged in' do
    create_list(:user, 3)

    result = gql_query(
      query: query,
      context: {
        current_user: nil,
        ability: ability_stub_true
      }).to_h.deep_symbolize_keys
    expect(result.dig(:data, :currentUser)).to be_nil
  end

  def query
    <<~GQL
      query {
        currentUser {
          id
          email
          authenticationToken
        }
      }
    GQL
  end
end
