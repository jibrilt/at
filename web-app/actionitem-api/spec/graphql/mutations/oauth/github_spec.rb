require 'spec_helper'

RSpec.describe Mutations::Oauth::Github do
  let(:user) { create(:user) }
  let(:github_access_token) {
    Faker::Alphanumeric.alphanumeric(number: 10)
  }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  it 'updates the access_token' do
    variables = {
      'code' => Faker::Alphanumeric.alphanumeric(number: 20)
    }

    allow_any_instance_of(described_class)
      .to receive(:github_access_token) do
        github_access_token
      end

    expect(user.github_providers.count).to eq(0)

    result = gql_query(
      query: mutation,
      variables: variables,
      context: {
        current_user: user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys.dig(
      :data,
      :githubOauth
    )

    expect(result[:success]).to eq(true)
    expect(result[:errors]).to eq([])

    expect(user.github_providers.count).to eq(1)

    expect(user.github_providers.first.access_token)
      .to eq(github_access_token)
  end

  it 'raises error if not permitted'  do
    variables = {
      'code' => Faker::Alphanumeric.alphanumeric(number: 20)
    }

    expect {
      gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: user,
          ability: ability_stub_false
        }
      )
    }.to raise_error GraphQL::Guard::NotAuthorizedError
  end

  def mutation
    <<~GQL
      mutation abstract(
        $code: String!
      ) {
        githubOauth(input: {
          code: $code
        }) {
          success
          errors
        }
      }
    GQL
  end
end
