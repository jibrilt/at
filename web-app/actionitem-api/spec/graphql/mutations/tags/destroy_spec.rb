require 'spec_helper'

RSpec.describe Mutations::Tags::Destroy do
  let(:tag) { create(:tag) }
  let(:user) { tag.user }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  it 'destroys a tag' do
    variables = {
      'gqlId' => tag.gql_id
    }

    result = nil

    expect {
      result = gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: user,
          ability: ability_stub_true
        }
      ).to_h.deep_symbolize_keys.dig(
        :data,
        :destroyTag
      )
    }.to change(Tag, :count).by(-1)

    expect(result[:success]).to be_truthy
    expect(result[:errors]).to be_blank
  end

  it 'raises error if GraphQL::Guard::NotAuthorizedError' do
    variables = {
      'gqlId' => tag.gql_id
    }

    expect {
      gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: user,
          ability: ability_stub_false
        }
      ).to_h.deep_symbolize_keys
    }.to raise_error(GraphQL::Guard::NotAuthorizedError)
  end

  def mutation
    <<~GQL
      mutation abstract(
        $gqlId: String!
      ) {
        destroyTag(input: {
          gqlId: $gqlId
        }) {
          success
          errors
        }
      }
    GQL
  end
end
