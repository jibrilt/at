require 'spec_helper'

RSpec.describe Mutations::Tags::Create do
  let(:tracked_item) { create(:tracked_item) }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  it 'creates a tag' do
    variables = {
      'name' => Faker::Alphanumeric.alphanumeric(number: 20),
      'trackeditemGqlId' => tracked_item.gql_id
    }

    result = nil

    expect {
      result = gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: tracked_item.user,
          ability: ability_stub_true
        }
      ).to_h.deep_symbolize_keys.dig(
        :data,
        :createTag
      )
    }.to change(Tag, :count).by(1)

    tag = Tag.last

    expect(
      result.dig(:tag, :id)
    ).to eq(tag.gql_id)

    expect(
      result.dig(:tag, :name)
    ).to eq(variables['name'])

    expect(
      result.dig(:tag, :createdAt)
    ).to eq(tag.created_at.iso8601)

    expect(
      result.dig(:tag, :updatedAt)
    ).to eq(tag.updated_at.iso8601)

    expect(
      result.dig(:tag, :trackedItem, :id)
    ).to eq(tracked_item.gql_id)

    expect(result[:success]).to be_truthy
    expect(result[:errors]).to be_blank
  end

  it 'raises error for RecordInvalid' do
    variables = {
      'name' => Faker::Alphanumeric.alphanumeric(number: 20),
      'trackeditemGqlId' => tracked_item.gql_id
    }

    tag = Tag.new
    tag.validate
    allow(Tag).to receive(:create!).
      and_raise(
        ActiveRecord::RecordInvalid.new(tag)
      )

    result = nil

    expect {
      result = gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: tracked_item.user,
          ability: ability_stub_true
        }
      ).to_h.deep_symbolize_keys
    }.to change(Tag, :count).by(0)

    expect(result[:errors]).to_not be_blank
    expect(result.dig(:errors, 0, :message)).
      to include(tag.errors.full_messages.first)
  end

  def mutation
    <<~GQL
      mutation abstract(
        $name: String!
        $trackeditemGqlId: String!
      ) {
        createTag(input: {
          name: $name,
          trackeditemGqlId: $trackeditemGqlId
        }) {
          tag {
            id
            name
            createdAt
            updatedAt

            trackedItem {
              id
            }
          }
          success
          errors
        }
      }
    GQL
  end
end
