 require 'spec_helper'

RSpec.describe Mutations::TrackedItems::Create do
  let(:user) { create(:user) }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:provider) { provider_whitelist.sample }

  before do
    allow_any_instance_of(described_class).to receive(:host_source).
      and_return(provider)
  end

  xit 'creates a tracked_item' do
    variables = {
      'providerThreadId' => Faker::Alphanumeric.alphanumeric(number: 20)
    }

    result = nil

    expect {
      result = gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: user,
          ability: ability_stub_true
        }
      ).to_h.deep_symbolize_keys.dig(
        :data,
        :createTrackedItem
      )
    }.to change(TrackedItem, :count).by(1)

    tracked_item = TrackedItem.first

    expect(
      result.dig(:trackedItem, :id)
    ).to eq(tracked_item.gql_id)

    expect(
      result.dig(:trackedItem, :providerThreadId)
    ).to eq(variables['providerThreadId'])

    expect(
      result.dig(:trackedItem, :provider)
    ).to eq(provider)

    expect(
      result.dig(:trackedItem, :user, :id)
    ).to eq(tracked_item.user.gql_id)

    expect(result[:success]).to be_truthy
    expect(result[:errors]).to be_blank
  end

  it 'raises error for RecordInvalid' do
    variables = {
      'providerThreadId' => Faker::Alphanumeric.alphanumeric(number: 20)
    }

    tracked_item = TrackedItem.new
    tracked_item.validate
    allow(TrackedItem).to receive(:create!).
      and_raise(
        ActiveRecord::RecordInvalid.new(tracked_item)
      )

    result = nil

    expect {
      result = gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: user,
          ability: ability_stub_true
        }
      ).to_h.deep_symbolize_keys
    }.to change(TrackedItem, :count).by(0)

    expect(result[:errors]).to_not be_blank
    expect(result.dig(:errors, 0, :message)).
      to include(tracked_item.errors.full_messages.first)
  end

  def mutation
    <<~GQL
      mutation abstract(
        $providerThreadId: String
      ) {
        createTrackedItem(input: {
          providerThreadId: $providerThreadId
        }) {
          trackedItem {
            id
            provider
            providerThreadId
            user {
              id
            }
          }
          success
          errors
        }
      }
    GQL
  end
end
