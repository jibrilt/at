require 'spec_helper'

RSpec.describe Mutations::TrackedItems::Update do
  let(:tracked_item) { create(:tracked_item) }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  it 'updates a tracked_item' do
    variables = {
      'id' => tracked_item.gql_id,
    }

    result = gql_query(
      query: mutation,
      variables: variables,
      context: {
        current_user: tracked_item.user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys.dig(:data, :updateTrackedItem)

    expect(result.dig(:trackedItem, :id)).to eq(tracked_item.gql_id)

    expect(result[:success]).to be_truthy
    expect(result[:errors]).to be_blank
  end

  it 'raises an error if invalid' do
    variables = {
      'id' => tracked_item.gql_id,
    }

    allow_any_instance_of(TrackedItem).to receive(:update!).
      and_raise(ActiveRecord::RecordInvalid.new(tracked_item))

    result = gql_query(
      query: mutation,
      variables: variables,
      context: {
        current_user: tracked_item.user,
        ability: ability_stub_true
      }
    ).to_h.deep_symbolize_keys

    expect(result[:success]).to_not be_truthy
    expect(result[:errors]).to_not be_blank
  end

  it 'raises error if not permitted'  do
    variables = {
      'id' => tracked_item.gql_id
    }

    expect {
      gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: tracked_item.user,
          ability: ability_stub_false
        }
      )
    }.to raise_error GraphQL::Guard::NotAuthorizedError
  end


  def mutation
    <<~GQL
      mutation abstract(
        $id: ID!
      ) {
        updateTrackedItem(input: {
          id: $id
        }) {
          trackedItem {
            id
          }
          success
          errors
        }
      }
    GQL
  end
end
