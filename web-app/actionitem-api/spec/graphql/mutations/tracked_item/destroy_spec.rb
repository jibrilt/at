require 'spec_helper'

RSpec.describe Mutations::TrackedItems::Destroy do
  let(:tracked_item) { create(:tracked_item) }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  it 'destroys a tracked_item' do
    variables = {
      'id' => tracked_item.gql_id,
    }

    result = nil

    expect {
      result = gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: tracked_item.user,
          ability: ability_stub_true
        }
      ).to_h.deep_symbolize_keys.dig(:data, :destroyTrackedItem)

      expect(result[:success]).to be_truthy
      expect(result[:errors]).to be_blank
    }.to change(TrackedItem, :count).by(-1)
  end

  it 'raises error if not permitted'  do
    variables = {
      'id' => tracked_item.gql_id
    }

    expect {
      gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: tracked_item.user,
          ability: ability_stub_false
        }
      )
    }.to raise_error GraphQL::Guard::NotAuthorizedError
  end


  def mutation
    <<~GQL
      mutation abstract(
        $id: ID!
      ) {
        destroyTrackedItem(input: {
          id: $id
        }) {
          success
          errors
        }
      }
    GQL
  end
end
