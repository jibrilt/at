require 'spec_helper'

RSpec.describe Mutations::TrackedItemNotifications::Clear do
  let(:tracked_item) { create(:tracked_item) }

  let(:tracked_item_notifications) {
    create_list(
      :tracked_item_notification,
      3,
      tracked_item: tracked_item
    )
  }

  let(:ability_stub_true) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(true)
    ability
  end

  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  it 'marks all trackeditem notifications as read' do
    variables = {
      'trackedItemGqlId' => tracked_item.gql_id
    }

    tracked_item_notifications.each do |notification|
      expect(notification.read?).to be false
    end

    result = gql_query(
      query: mutation,
      variables: variables,
      context: {
        current_user: tracked_item.user,
        ability: ability_stub_true
      }
    )

    tracked_item_notifications.each do |notification|
      expect(notification.reload.read?).to be true
    end
  end

  it 'raises an error if not permitted'  do
    variables = {
      'trackedItemGqlId' => tracked_item.gql_id
    }

    expect {
      gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: tracked_item.user,
          ability: ability_stub_false
        }
      )
    }.to raise_error GraphQL::Guard::NotAuthorizedError
  end

  def mutation
    <<~GQL
      mutation abstract(
        $trackedItemGqlId: ID!
      ) {
        clearTiNotifications(input: {
          trackedItemGqlId: $trackedItemGqlId
        }) {
          success
          errors
        }
      }
    GQL
  end
end
