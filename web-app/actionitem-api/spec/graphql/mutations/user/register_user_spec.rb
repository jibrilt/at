require 'spec_helper'

RSpec.describe Mutations::Users::RegisterUser do
  let(:ability_stub_false) do
    ability = double('ability')
    allow(ability).to receive(:can?).and_return(false)
    ability
  end

  it 'registers the user' do
    variables = {
      'email' => Faker::Internet.email,
      'password' => 'testing123'
    }

    result = gql_query(
        query: mutation,
        variables: variables,
        context: {
          current_user: nil,
          ability: ability_stub_false
        }
      ).
      to_h.deep_symbolize_keys.dig(:data, :registerUser)

    user = User.first
    expect(result.dig(:user, :id)).to eq(user.gql_id)
    expect(result.dig(:user, :email)).to eq(variables['email'])
    expect(result[:errors]).to be_blank
  end

  it 'raises error for RecordInvalid' do
    variables = {
      'email' => Faker::Internet.email,
      'password' => 'testing123'
    }

    user = User.new
    user.validate # missing fields makes this invalid
    allow(User).to receive(:create!).
      and_raise(ActiveRecord::RecordInvalid.new(user))
    result = gql_query(
      query: mutation,
      variables: variables,
      context: {
        current_user: nil,
        ability: ability_stub_false
      }).to_h.deep_symbolize_keys

    expect(result[:errors]).to_not be_blank
    expect(result.dig(:errors, 0, :message)).
      to include(user.errors.full_messages.first)
  end

  def mutation
    <<~GQL
      mutation abstract(
        $email: String!,
        $password: String!,
      ) {
        registerUser(input: {
          email: $email,
          password: $password,
        }) {
          user {
            id
            email
            authenticationToken
          }
          success
          errors
        }
      }
    GQL
  end
end
