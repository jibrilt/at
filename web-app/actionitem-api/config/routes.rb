Rails.application.routes.draw do
  devise_for :users, skip: :sessions

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql'
  end

  mount ActionCable.server, at: '/cable'

  post '/graphql', to: 'graphql#execute'

  namespace :provider_webhooks do
    post '/github_payload', to: 'github#payload'
  end

  post '/google-notifications', to: 'gmail_tracking/push_notifications#create'
end
