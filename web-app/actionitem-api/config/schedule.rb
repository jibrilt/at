every 24.hours do
  rake 'refresh_user_watch_access'
end

ENV.each_key do |key|
  env key.to_sym, ENV[key]
end

set :environment, ENV['RAILS_ENV']


if environment == 'production' || environment == 'staging'
  every :day do
    rake 'regen_aloglia_user_keys'
  end
end
