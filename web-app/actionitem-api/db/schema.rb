# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_31_212643) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "gmail_message_histories", force: :cascade do |t|
    t.string "history_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "push_notification", default: false
    t.index ["history_id"], name: "index_gmail_message_histories_on_history_id"
    t.index ["user_id"], name: "index_gmail_message_histories_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "tracked_item_id", null: false
    t.string "provider_message_id", null: false
    t.string "deep_link"
    t.text "body"
    t.datetime "provider_date_posted"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "provider_date_posted_i"
    t.index ["tracked_item_id"], name: "index_messages_on_tracked_item_id"
  end

  create_table "oauth_providers", force: :cascade do |t|
    t.string "provider"
    t.string "access_token"
    t.string "refresh_token"
    t.string "uid"
    t.bigint "user_id", null: false
    t.datetime "refresh_token_expiry"
    t.index ["provider"], name: "index_oauth_providers_on_provider"
    t.index ["user_id"], name: "index_oauth_providers_on_user_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.bigint "tracked_item_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_tags_on_name"
    t.index ["tracked_item_id"], name: "index_tags_on_tracked_item_id"
    t.index ["user_id"], name: "index_tags_on_user_id"
  end

  create_table "tracked_item_notifications", force: :cascade do |t|
    t.string "aasm_state", default: "unread"
    t.string "notification_type"
    t.string "provider_abstract_id"
    t.bigint "tracked_item_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tracked_item_id"], name: "index_tracked_item_notifications_on_tracked_item_id"
  end

  create_table "tracked_items", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "provider"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "provider_thread_id"
    t.string "initial_from_email"
    t.string "initial_from"
    t.string "initial_subject"
    t.datetime "latest_message_provider_date"
    t.index ["user_id", "provider", "provider_thread_id"], name: "index_creator_creation_provider_ids", unique: true
    t.index ["user_id"], name: "index_tracked_items_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "authentication_token"
    t.datetime "authentication_token_created_at"
    t.string "name"
    t.datetime "last_online_at"
    t.string "algolia_api_key"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "oauth_providers", "users"
  add_foreign_key "tags", "tracked_items"
  add_foreign_key "tags", "users"
end
