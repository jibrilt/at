class RenameAskerToCreator < ActiveRecord::Migration[6.0]
  def change
    rename_column :tasks, :asker_id, :creator_id
  end
end
