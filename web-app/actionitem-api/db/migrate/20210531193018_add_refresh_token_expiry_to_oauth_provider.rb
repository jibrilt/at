class AddRefreshTokenExpiryToOauthProvider < ActiveRecord::Migration[6.0]
  def change
    add_column :oauth_providers, :refresh_token_expiry, :datetime
  end
end
