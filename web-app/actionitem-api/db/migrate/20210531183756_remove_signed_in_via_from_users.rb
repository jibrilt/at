class RemoveSignedInViaFromUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :signed_in_via, :integer, default: 0
  end
end
