class RemoveThreadIdFromTrackedItem < ActiveRecord::Migration[6.0]
  def change
    remove_column :tracked_items, :thread_id, :string, index: true
  end
end
