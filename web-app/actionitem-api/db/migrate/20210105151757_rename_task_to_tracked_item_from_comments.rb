class RenameTaskToTrackedItemFromComments < ActiveRecord::Migration[6.0]
  def change
    rename_column :comments, :task_id, :tracked_item_id
  end
end
