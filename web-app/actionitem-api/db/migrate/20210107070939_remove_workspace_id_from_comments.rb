class RemoveWorkspaceIdFromComments < ActiveRecord::Migration[6.0]
  def change
    remove_column :comments, :workspace_id, :bigint
  end
end
