class CreateGmailMessageHistory < ActiveRecord::Migration[6.0]
  def change
    create_table :gmail_message_histories do |t|
      t.string :history_id, index: true
      t.references :user, null: false

      t.timestamps
    end
  end
end
