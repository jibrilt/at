class CreateComments < ActiveRecord::Migration[6.0]
  def up
    create_table :comments, partition_key: :workspace_id do |t|
      t.text :description
      t.references :task, null: false
      t.references :user, null: false
      t.references :workspace, null: false

      t.timestamps null: false
    end

    create_distributed_table :comments, :workspace_id
  end

  def down
    drop_table :comments
  end
end
