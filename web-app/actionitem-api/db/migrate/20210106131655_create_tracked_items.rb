class CreateTrackedItems < ActiveRecord::Migration[6.0]
  def change
    create_table :tracked_items, partition_key: :workspace_id do |t|
      t.text :description
      t.references :creator, null: false
      t.string :aasm_state
      t.references :workspace, null: false
      t.string :creation_origin
      t.string :thread_id, index: true

      t.timestamps
    end

    create_distributed_table :tasks, :workspace_id
  end
end
