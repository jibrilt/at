class FixMissingPrimaryKeys < ActiveRecord::Migration[6.0]
  def up
    execute 'ALTER TABLE tracked_items ADD PRIMARY KEY (id);'
    execute 'ALTER TABLE comments ADD PRIMARY KEY (id);'
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
