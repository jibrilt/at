class AddLastOnlineAtToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :last_online_at, :datetime
  end
end
