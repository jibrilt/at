class DropTaskAssignment < ActiveRecord::Migration[6.0]
  def up
    drop_table :task_assignments
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
