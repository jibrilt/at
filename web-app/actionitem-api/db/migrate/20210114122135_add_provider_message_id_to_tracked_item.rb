class AddProviderMessageIdToTrackedItem < ActiveRecord::Migration[6.0]
  def change
    add_column :tracked_items, :provider_message_id, :string, index: true
  end
end
