class CreateMessage < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      t.references :tracked_item, null: false
      t.references :user, null: false

      t.string :provider_message_id, null: false
      t.string :subject
      t.string :deep_link
      t.text :body
      t.datetime :date
      t.string :from

      t.timestamps
    end
  end
end
