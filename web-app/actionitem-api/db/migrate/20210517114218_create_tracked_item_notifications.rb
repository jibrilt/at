class CreateTrackedItemNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :tracked_item_notifications do |t|
      t.string :aasm_state, default: "unread"
      t.string :notification_type

      t.string :provider_abstract_id
      t.references :tracked_item

      t.timestamps
    end
  end
end
