class AddLatestMessageProviderDateToTrackedItem < ActiveRecord::Migration[6.0]
  def up
    add_column :tracked_items, :latest_message_provider_date, :datetime

    TrackedItem.all.each do |tracked_item|
      tracked_item.update!(
        latest_message_provider_date: tracked_item
          .latest_message
          .provider_date_posted
      )
    end
  end

  def down
    remove_column :tracked_items, :latest_message_provider_date
  end
end
