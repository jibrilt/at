class CreateTaskAssignment < ActiveRecord::Migration[6.0]
  def up
    create_table :task_assignments, partition_key: :workspace_id do |t|
      t.bigint :assigned_to_id, null: false
      t.bigint :task_id, null: false
      t.references :workspace, null: false

      t.timestamps
    end
    add_index :task_assignments, [:assigned_to_id, :task_id], unique: true

    create_distributed_table :task_assignments, :workspace_id
  end

  def down
    drop_table :task_assignments
  end
end
