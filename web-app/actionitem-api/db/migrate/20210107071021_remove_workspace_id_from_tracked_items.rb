class RemoveWorkspaceIdFromTrackedItems < ActiveRecord::Migration[6.0]
  def change
    remove_column :tracked_items, :workspace_id, :bigint
  end
end
