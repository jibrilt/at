class CreateProviders < ActiveRecord::Migration[6.0]
  def change
    create_table :providers do |t|
      t.string :name
      t.integer :provider_type, default: 0
      t.string :token
      t.string :bot_token
      t.string :refresh_token
      t.string :uid
      t.references :user

      t.timestamps
    end

    add_column :users, :name, :string
    add_column :users, :signed_in_via, :integer, default: 0
  end
end
