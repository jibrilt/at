class AddPushNotificationToGmailMessageHistory < ActiveRecord::Migration[6.0]
  def change
    add_column :gmail_message_histories, :push_notification, :boolean, default: false
  end
end
