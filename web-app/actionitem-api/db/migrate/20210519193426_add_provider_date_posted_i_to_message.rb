class AddProviderDatePostedIToMessage < ActiveRecord::Migration[6.0]
  def up
    add_column :messages, :provider_date_posted_i, :bigint

    Message.all.each do |message|
      message.update!(
        provider_date_posted_i: message.provider_date_posted.to_i
      )
    end
  end

  def down
    remove_column :messages, :provider_date_posted_i
  end
end
