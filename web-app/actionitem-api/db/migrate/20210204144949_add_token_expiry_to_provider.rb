class AddTokenExpiryToProvider < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :token_expiry, :datetime
  end
end
