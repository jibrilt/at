class MoveTrackedItemAttrsFromMessageToTrackedItem < ActiveRecord::Migration[6.0]
  def up
    remove_column :messages, :provider
    remove_column :messages, :subject
    remove_column :messages, :user_id
    remove_column :messages, :from
    remove_column :messages, :from_email

    rename_column :messages, :date, :provider_date_posted

    add_column :tracked_items, :initial_from_email, :string
    add_column :tracked_items, :initial_from, :string
    add_column :tracked_items, :initial_subject, :string
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
