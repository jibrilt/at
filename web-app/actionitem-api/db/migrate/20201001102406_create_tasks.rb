class CreateTasks < ActiveRecord::Migration[6.0]
  def up
    create_table :tasks, partition_key: :workspace_id do |t|
      t.string :name, null: false
      t.text :description
      t.references :asker, null: false
      t.string :aasm_state
      t.datetime :due_at
      t.datetime :completed_at
      t.references :workspace, null: false
      t.string :creation_origin

      t.timestamps
    end

    create_distributed_table :tasks, :workspace_id
  end

  def down
    drop_table :tasks
  end
end
