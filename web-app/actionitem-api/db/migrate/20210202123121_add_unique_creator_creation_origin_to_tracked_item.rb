class AddUniqueCreatorCreationOriginToTrackedItem < ActiveRecord::Migration[6.0]
  def change
    add_index :tracked_items,
      [:creator_id, :creation_origin, :provider_message_id],
      unique: true,
      name: 'index_creator_creation_provider_ids'
  end
end
