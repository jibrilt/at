class DropWorkspacesTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :workspaces
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
