class CreateGmailMessage < ActiveRecord::Migration[6.0]
  def change
    create_table :gmail_messages do |t|
      t.string :message_id
      t.string :thread_id
      t.string :subject
      t.text :body
      t.text :body_short
      t.datetime :date
      t.string :from
      t.string :to

      t.references :tracked_item, null: false

      t.timestamps
    end
  end
end
