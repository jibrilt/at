class CreateOauthProvider < ActiveRecord::Migration[6.0]
  def change
    create_table :oauth_providers do |t|
      t.string :provider
      t.string :access_token
      t.string :refresh_token
      t.string :uid
      t.references :user, null: false, foreign_key: true
    end
  end
end
