class DropPendingInvite < ActiveRecord::Migration[6.0]
  def up
    drop_table :pending_invites
  end

  def down
    create_table :pending_invites do |t|
      t.string :email
      t.references :workspace, null: false, foreign_key: true
      t.string :aasm_state

      t.timestamps
    end
  end
end
