class UpdateTrackedItemToProvider < ActiveRecord::Migration[6.0]
  def up
    remove_column :tracked_items, :description
    remove_column :tracked_items, :aasm_state

    rename_column :tracked_items, :creator_id, :user_id
    rename_column :tracked_items, :creation_origin, :provider
    rename_column :tracked_items, :provider_message_id, :provider_thread_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
