class DestroyAllTrackedItems < ActiveRecord::Migration[6.0]
  def up
    TrackedItem.destroy_all
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
