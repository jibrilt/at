class AddSlackToWorkspace < ActiveRecord::Migration[6.0]
  def change
    add_column :workspaces, :slack_team_domain, :string
    add_column :workspaces, :slack_team_id, :string

    add_column :workspaces, :encrypted_slack_token, :string
    add_column :workspaces, :encrypted_slack_token_iv, :string
  end
end
