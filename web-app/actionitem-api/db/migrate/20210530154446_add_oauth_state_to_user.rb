class AddOauthStateToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :oauth_state, :string, index: true
  end
end
