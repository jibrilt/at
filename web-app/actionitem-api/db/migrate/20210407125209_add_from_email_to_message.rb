class AddFromEmailToMessage < ActiveRecord::Migration[6.0]
  def change
    add_column :messages, :from_email, :string
  end
end
