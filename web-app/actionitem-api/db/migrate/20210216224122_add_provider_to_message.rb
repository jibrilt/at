class AddProviderToMessage < ActiveRecord::Migration[6.0]
  def change
    add_column :messages, :provider, :string
  end
end
