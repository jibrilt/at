class AddAlgoliaApiKeyToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :algolia_api_key, :string
  end
end
