class AddThreadIdToTasks < ActiveRecord::Migration[6.0]
  def change
    add_column :tasks, :thread_id, :string, index: true
  end
end
