class RemoveOauthStateFromUser < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :oauth_state, :string
  end
end
