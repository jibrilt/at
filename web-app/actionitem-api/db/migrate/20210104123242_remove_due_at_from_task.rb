class RemoveDueAtFromTask < ActiveRecord::Migration[6.0]
  def change
    remove_column :tasks, :due_at, :datetime
  end
end
