class CreatePendingInvite < ActiveRecord::Migration[6.0]
  def change
    create_table :pending_invites do |t|
      t.string :email
      t.references :workspace, null: false, foreign_key: true
      t.string :aasm_state

      t.timestamps
    end
  end
end
