class AddIndexToOauthProvider < ActiveRecord::Migration[6.0]
  def change
    add_index :oauth_providers, :provider
  end
end
