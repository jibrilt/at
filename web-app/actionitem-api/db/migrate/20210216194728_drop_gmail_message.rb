class DropGmailMessage < ActiveRecord::Migration[6.0]
  def up
    drop_table :gmail_messages
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
