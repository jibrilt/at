require 'faker'

user = User.find_by(
  email: 'tester123@gmail.com'
)

if user.blank?
  user = User.create(
    email: 'tester123@gmail.com',
    password: 'tester123',
    password_confirmation: 'tester123'
  )
end

# Create Trackeditems and Messages
['gmail', 'slack', 'github', 'todoist'].each do |provider|
  20.times.each do |n|
    ActionItemApi::TrackedItemFactory.create(
      user: user,
      provider: provider,
      provider_thread_id: SecureRandom.hex(12),
      provider_message_id: SecureRandom.hex(12),
      subject: Faker::Company.bs,
      deep_link: 'www.google.com',
      body: Faker::Lorem.paragraph(sentence_count: 20),
      date: Time.at(Random.rand((Time.now - (60*60*24*365*5)).to_f..Time.now.to_f)),
      from: Faker::Name.unique.name,
      from_email: Faker::Internet.email
    )
  end
end

Message.reindex
