import gql from 'graphql-tag';
import { results } from '@/graphql/queries/local';

import { trackedItemsConnection } from '@/graphql/queries/trackedItemsConnection';

export const typeDefs = gql`
  extend type Query {
    isAuthenticated: Boolean!
    results: [Result]
    resultGroups: [ResultGroup]
    searchBackdrop: Boolean!
    notificationsBackdrop: Boolean!
  }

  type ResultGroup {
    id: String!
    group: String!
    page: Int!
    groupProvider: String!
  }

  type Result {
    id: String!
    gqlId: String!
    subject: String!
    senderName: String!
    timeStamp: String!
    body: String!
    provider: String!
    fromEmail: String!
    deepLink: String!
    active: Boolean!
    resultsGroup: String!
    groupIndex: Int!
  }
`;

export const resolvers = {
  Mutation: {

    // Result

    addResult: (_root, variables, { cache, getCacheKey }) => {
      const data = cache.readQuery({ query: results});

      data.results.push({
        __typename: 'Result',
        id: variables.id,
        gqlId: variables.gql_id,
        subject: variables.subject,
        senderName: variables.senderName,
        timeStamp: variables.timeStamp,
        body: variables.body,
        provider: variables.provider,
        fromEmail: variables.fromEmail,
        deepLink: variables.deepLink,
        active: variables.active || false,
        resultsGroup: variables.resultsGroup,
        groupIndex: variables.groupIndex
      });

      cache.writeQuery({ query: results, data });

      return null;
    },
    clearResults: (_root, variables, { cache, getCacheKey }) => {
      cache.writeData({
        data: {
          results: [],
          resultGroups: []
        }
      });
    },
    updateResultActive: (_root, variables, { cache, getCacheKey }) => {
      const id = getCacheKey(
        {
          __typename: 'Result',
          id: variables.id
        }
      );

      const data = {
        active: variables.active
      };

      cache.writeData({ id, data });

      return null;
    },

    // Result Providers

    addResultGroups: (_root, variables, { cache, getCacheKey }) => {
      cache.writeData({
        data: {
          resultGroups: variables.groups
        }
      });

      return null;
    },
    updateResultGroup: (_root, variables, { cache, getCacheKey }) => {
      const id = getCacheKey(
        {
          __typename: 'ResultGroup',
          id: variables.id
        }
      );

      const data = {
        page: variables.page
      };

      cache.writeData({
        id,
        data
      });

      return null;
    },

    // searchBackdrop

    updateSearchBackdrop: (_root, variables, { cache, getCacheKey }) => {
      cache.writeData({
        data: {
          searchBackdrop: variables.enabled
        }
      });

      return null;
    },

    // notificationsBackdrop

    updateNotificationsBackdrop: (_root, variables, { cache, getCacheKey }) => {
      cache.writeData({
        data: {
          notificationsBackdrop: variables.enabled
        }
      });

      return null;
    },

    // trackedItemsConnection (aka: TrackedItem list)

    updtTrckedItmNotfction: (_root, variables, { cache, getCacheKey }) => {
      const newQuery = cache
        .readQuery({ query: trackedItemsConnection });

      let edges = newQuery
        .trackedItemsConnection
        .edges;

      const targetEdgeIndex = edges.findIndex(
        edge => edge.node.id == variables.id
      );

      edges[targetEdgeIndex]
        .node
        .unreadNotificationsToS = variables.notification;

      cache.writeQuery({
        query: trackedItemsConnection,
        data: { trackedItemsConnection: newQuery.trackedItemsConnection }
      });
    },

    removeTrckedItm: (_root, variables, { cache, getCacheKey }) => {
      const newQuery = cache
        .readQuery({ query: trackedItemsConnection });

      let edges = newQuery
        .trackedItemsConnection
        .edges;

      const targetEdgeIndex = edges.findIndex(
        edge => edge.node.id == variables.id
      );

      edges.splice(targetEdgeIndex, 1);

      cache.writeQuery({
        query: trackedItemsConnection,
        data: { trackedItemsConnection: newQuery.trackedItemsConnection }
      });
    },

    removeTrckedItmLabel: (_root, variables, { cache, getCacheKey }) => {
      const newQuery = cache
        .readQuery({ query: trackedItemsConnection });

      let edges = newQuery
        .trackedItemsConnection
        .edges;

      const targetEdgeIndex = edges.findIndex(
        edge => edge.node.id == variables.trackedItemId
      );

      let tags = edges[targetEdgeIndex]
        .node
        .tags

      const targetTagIndex = tags.findIndex(
        tag => tag.id == variables.tagId
      );

      tags.splice(targetTagIndex, 1);

      cache.writeQuery({
        query: trackedItemsConnection,
        data: { trackedItemsConnection: newQuery.trackedItemsConnection }
      });
    }
  }
};
