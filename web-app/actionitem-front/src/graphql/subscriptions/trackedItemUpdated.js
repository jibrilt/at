import gql from 'graphql-tag';
import { trackedItemListFields } from '@/graphql/fragments/trackedItemListFields.js'

export const trackedItemUpdated = gql`
  ${trackedItemListFields}

  subscription {
    trackedItemUpdated {
      trackedItemGqlId
      trackedItem {
        edges {
          cursor
          node {
            ...TrackedItemListFields
          }
        }
      }
      eventType
    }
  }
`;
