import gql from 'graphql-tag';

export const currentUser = gql`
  query {
    currentUser {
      id
      email
      authenticationToken
      algoliaApiKey
    }
  }
`;

export const isAuthenticated = gql`
  query isAuthenticated {
    isAuthenticated @client
  }
`;
