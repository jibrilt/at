import gql from 'graphql-tag';

const mutation = gql`
  mutation updateResultGroup(
    $id: String!,
    $page: Int!
  ) {
    updateResultGroup(
      id: $id,
      page: $page,
    ) @client
  }
`;

export default function updateResultGroup({
  apollo,
  id,
  page,
}) {
  return apollo.mutate({
    mutation,
    variables: {
      id,
      page,
    }
  });
}
