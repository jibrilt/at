import gql from 'graphql-tag';

const mutation = gql`
  mutation removeTrckedItm($id: String!) {
    removeTrckedItm(
      id: $id
    ) @client
  }
`;

export default function removeTrckedItm({
  apollo,
  id
}) {
  return apollo.mutate({
    mutation,
    variables: {
      id
    }
  });
}
