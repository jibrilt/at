import gql from 'graphql-tag';

const mutation = gql`
  mutation updtTrckedItmNotfction($id: String!, $notification: String!) {
    updtTrckedItmNotfction(
      id: $id,
      notification: $notification
    ) @client
  }
`;

export default function updtTrckedItmNotfction({
  apollo,
  id,
  notification
}) {
  return apollo.mutate({
    mutation,
    variables: {
      id,
      notification
    },
  });
}
