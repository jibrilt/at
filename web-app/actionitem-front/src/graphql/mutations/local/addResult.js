import gql from 'graphql-tag';

const mutation = gql`
  mutation addResult(
    $id: String!,
    $subject: String!,
    $senderName: String!,
    $timeStamp: String!,
    $body: String!,
    $provider: String!,
    $fromEmail: String!,
    $deepLink: String!,
    $resultsGroup: String!,
    $active: Boolean!,
    $groupIndex: Int
  ) {
    addResult(
      id: $id,
      subject: $subject,
      senderName: $senderName,
      timeStamp: $timeStamp,
      body: $body,
      provider: $provider,
      fromEmail: $fromEmail,
      deepLink: $deepLink,
      resultsGroup: $resultsGroup,
      active: $active,
      groupIndex: $groupIndex
    ) @client
  }
`;

export default function addResult({
  apollo,
  id,
  subject,
  senderName,
  timeStamp,
  body,
  provider,
  fromEmail,
  deepLink,
  resultsGroup,
  active,
  groupIndex
}) {
  return apollo.mutate({
    mutation,
    variables: {
      id,
      subject,
      senderName,
      timeStamp,
      body,
      provider,
      fromEmail,
      deepLink,
      resultsGroup,
      active,
      groupIndex
    },
  });
}
