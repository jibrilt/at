import gql from 'graphql-tag';

const mutation = gql`
  mutation clearResults {
    clearResults @client
  }
`;

export default function clearResults({
  apollo
}) {
  return apollo.mutate({
    mutation,
    variables: {
    }
  });
}
