import gql from 'graphql-tag';

const mutation = gql`
  mutation updateSearchBackdrop($enabled: Boolean!) {
    updateSearchBackdrop(
      enabled: $enabled
    ) @client
  }
`;

export default function updateSearchBackdrop({
  apollo,
  enabled
}) {
  return apollo.mutate({
    mutation,
    variables: {
      enabled
    },
  });
}
