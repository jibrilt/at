import gql from 'graphql-tag';

const mutation = gql`
  mutation abstract(
    $trackedItemGqlId: ID!
  ) {
    clearTiNotifications(input: {
      trackedItemGqlId: $trackedItemGqlId
    }) {
      success
      errors
    }
  }
`;

export default function deleteProvider({
  apollo,
  trackedItemGqlId
}) {
  return apollo.mutate({
    mutation,
    variables: {
      trackedItemGqlId
    },
  });
}
