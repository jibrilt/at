import gql from 'graphql-tag';

const mutation = gql`
  mutation abstract(
    $code: String!
  ) {
    githubOauth(input: {
      code: $code
    }) {
      success
      errors
    }
  }
`;

export default function githubOauth({
  apollo,
  code
}) {
  return apollo.mutate({
    mutation,
    variables: {
      code
    }
  });
}
