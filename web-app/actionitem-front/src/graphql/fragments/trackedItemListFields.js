import gql from 'graphql-tag';

export const trackedItemListFields = gql`
  fragment TrackedItemListFields on TrackedItem {
    id
    provider
    initialSubject
    initialFrom
    initialFromEmail
    unreadNotificationsToS

    tags {
      id
      name
    }

    latestMessage {
      providerDatePosted
      body
      deepLink
    }
  }
`;
