import { OAUTH2_STATE_KEY } from '@/common/config';

export const getState = () => {
  return window.localStorage.getItem(OAUTH2_STATE_KEY);
};

export const saveState = token => {
  window.localStorage.setItem(OAUTH2_STATE_KEY, token);
};

export const destroyState = () => {
  window.localStorage.removeItem(OAUTH2_STATE_KEY);
};

export const parseOrigin = (state) => {
  return `${state.split('|')[0]}`;
};

export const parseProvider = (state) => {
  return `${state.split('|')[1]}`;
};

export const parseState = (state) => {
  return `${state.split('|')[2]}`;
};

export const mergeOriginProvider = (origin, provider, state) => {
  return `${origin}|${provider}|${state}`;
}

export default {
  getState,
  saveState,
  destroyState,
  parseOrigin,
  parseProvider,
  parseState,
  mergeOriginProvider
};
