export const API_URL = "http://localhost:3000";
export const AUTH_TOKEN_KEY = 'authToken';
export const OAUTH2_STATE_KEY = 'oauth2State';

export const ACTIONITEM_PROVIDERS = [
    'slack', 'gmail','github',
    'asana', 'outlook', 'jira',
    'teams', 'todoist'
  ];

export const ACTIONITEM_GROUPS = [
  'Slack Items', 'Gmail Items','Github Items',
  'Asana Items', 'Outlook Items', 'Jira Items',
  'Teams Items', 'Todoist Items'
]
