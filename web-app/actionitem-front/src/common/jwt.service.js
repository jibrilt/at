import { AUTH_TOKEN_KEY } from '@/common/config';

export const getToken = () => {
  return window.localStorage.getItem(AUTH_TOKEN_KEY);
};

export const saveToken = token => {
  window.localStorage.setItem(AUTH_TOKEN_KEY, token);
};

export const destroyToken = () => {
  window.localStorage.removeItem(AUTH_TOKEN_KEY);
};

export default { getToken, saveToken, destroyToken };
