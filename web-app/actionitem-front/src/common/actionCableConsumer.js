import ActionCable from 'actioncable';
import JwtService from "@/common/jwt.service";

const getCableUrl = () => {
  const protocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:';
  const host = 'localhost'; //window.location.hostname;
  const authToken = JwtService.getToken();
  return `${protocol}//${host}:3000/cable?token=${authToken}`;
};

const cable = ActionCable.createConsumer(getCableUrl());
const cableUrl = cable.url;

Object.defineProperty(cable, 'url', {
  get: function() {
    return getCableUrl();
  },
});

export default cable;
