import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      name: "home",
      path: "/",
      meta: {
        title: 'Open Items',
        loggedInOnly: true
      },
      component: () => import("@/views/Home")
    },
    {
      name: "login",
      path: "/login",
      meta: {
        disableIfLoggedIn: true,
        title: 'Login to your Account'
      },
      component: () => import("@/views/Login")
    },
    {
      name: "register",
      path: "/register",
      meta: {
        disableIfLoggedIn: true,
        title: 'Create an account with us!'
      },
      component: () => import("@/views/Register")
    },
    {
      name: "forgot-password",
      path: "/forgot-password",
      meta: {
        disableIfLoggedIn: true,
        title: 'Forgot Password'
      },
      component: () => import("@/views/ForgotPassword")
    },
    {
      name: "account",
      path: "/account",
      meta: {
        loggedInOnly: true,
        title: 'Account Settings'
      },
      component: () => import("@/views/Account")
    }
  ]
});
