export default {
  methods: {
    resultFuseIds(id, group) {
      return `${id}-${this.replaceSpcWthDashes(group)}`;
    },
    replaceSpcWthDashes(string) {
      return string.replace(/\s+/g, '-');
    }
  }
}
