import Vue from 'vue';
import VueApollo from 'vue-apollo';
import { BootstrapVue } from 'bootstrap-vue';
import VScrollLock from 'v-scroll-lock'

import App from './App.vue';
import router from './router';
import apolloClient from '@/common/apolloClient';
import { currentUser } from '@/graphql/queries/auth';
import JwtService from "@/common/jwt.service";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueApollo);
Vue.use(VScrollLock);

const apolloProvider = new VueApollo({ defaultClient: apolloClient });

router.beforeEach((to, from, next) => {
  // Check authentication before each pageload
  let authTokenPresent = !!JwtService.getToken();
  if (authTokenPresent) {
    new Promise(resolve => {
      apolloClient.query({
        query: currentUser,
        fetchPolicy: 'no-cache'
      }).then(({ data }) => {
        apolloClient.writeQuery({ query: currentUser, data });
        if(data.currentUser === null ) {
          JwtService.destroyToken();
          apolloClient.resetStore();
          apolloClient.writeData({
            data: {
              isAuthenticated: !!JwtService.getToken()
            }
          });
          return next({name: 'home'});
        }
      });
    });
  }

  if (to.meta.disableIfLoggedIn && !!JwtService.getToken()) {
    return next({name: 'login'});
  } else if (to.meta.loggedInOnly && !JwtService.getToken()) {
    return next({name: 'login'});
  }

  next();
});

router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = `${to.meta.title} - ActionItem Prototype`;
  });
});

new Vue({
  router,
  apolloProvider,
  apollo: {
    currentUser: currentUser
  },
  render: h => h(App)
}).$mount('#app');
