# Chrome Extension

Getting Started with the Action Item Chrome Extension
----------------------

### Install dependencies

It is recommended to install Yarn through the npm package manager, which comes bundled with Node.js when you install it on your system.

Once you have npm installed. Select node version `14.16.1` then you can run the following to install or upgrade Yarn:

```
npm install --global yarn
```

Double check if yarn is installed:

```
yarn -v
```

### Install packages

```
yarn install
```

### Compiles and hot-reloads for development. This will also generate the disribution folder.
```
yarn serve
```

Note: if you encounter any errors during compilation. Try running the command `yarn serve` again.

### From the chrome extensions page (chrome://extensions/), toggle developer mode

### Click `Load Unpacked`.

### Select the distribution folder. EG: `actionitem/chrome-extension/dist`.

### Make sure the API is also running. Check the project's READ_ME for more information.


Production Setup
----------------------

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
