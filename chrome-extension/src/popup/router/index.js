import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      name: "home",
      path: "/",
      meta: {
        title: 'Welcome Home!'
      },
      component: () => import("@/popup/views/Home.vue")
    }
  ]
});
