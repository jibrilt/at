import Vue from 'vue';
import VueApollo from 'vue-apollo';
import { BootstrapVue } from 'bootstrap-vue';

import App from './App.vue';
import router from './router';

import apolloClient from '@/common/apolloClient';
import { currentUser } from '@/graphql/queries/auth';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueApollo);

const apolloProvider = new VueApollo({ defaultClient: apolloClient });

/* eslint-disable no-new */

new Vue({
  router,
  apolloProvider,
  apollo: {
    currentUser: currentUser
  },
  render: h => h(App)
}).$mount('#app');


