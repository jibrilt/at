import { isAuthenticated, currentUser } from '@/graphql/queries/auth';

export default {
  apollo: {
    isAuthenticated: isAuthenticated,
    currentUser: currentUser
  },
  computed: {
    isLoggedIn() {
      return this.currentUser != null && this.isAuthenticated;
    }
  }
};
