//browser.runtime.onMessage.addListener(function (request, sender, sendResponse) {
//  browser.tabs.executeScript({
//    file: 'js/content-script.js'
//  });
//});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  if (changeInfo.status !== 'complete' || !tab.url) return;

  const url = new URL(tab.url);
  const domain = url.hostname;

  if (domain !== 'mail.google.com') return;

  const executing = browser.tabs.executeScript(
    tabId,
    {
      file: 'js/content-script.js'
    }
  );
});
