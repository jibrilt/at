import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      name: "home",
      path: "/override.html",
      meta: {
        title: 'Open Items',
        loggedInOnly: true
      },
      component: () => import("@/views/startpage/Home")
    },
    {
      name: "login",
      path: "/login",
      meta: {
        disableIfLoggedIn: true,
        title: 'Login to your Account'
      },
      component: () => import("@/views/startpage/Login")
    },
    {
      name: "register",
      path: "/register",
      meta: {
        disableIfLoggedIn: true,
        title: 'Create an account with us!'
      },
      component: () => import("@/views/startpage/Register")
    },
    {
      name: "forgot-password",
      path: "/forgot-password",
      meta: {
        disableIfLoggedIn: true,
        title: 'Forgot Password'
      },
      component: () => import("@/views/startpage/ForgotPassword")
    },
    {
      name: "account",
      path: "/account",
      meta: {
        loggedInOnly: true,
        title: 'Account Settings'
      },
      component: () => import("@/views/startpage/Account")
    },
    {
      name: "auth",
      path: "/auth",
      meta: {
        loggedInOnly: false,
        title: 'Auth'
      },
      component: () => import("@/views/startpage/Auth")
    },
    {
      name: "slack",
      path: "/slack",
      meta: {
        loggedInOnly: true,
        title: 'Slack'
      },
      component: () => import("@/views/startpage/Slack")
    },
    {
      name: "google",
      path: "/google",
      meta: {
        loggedInOnly: true,
        title: 'Google'
      },
      component: () => import("@/views/startpage/Google")
    }
  ]
});
