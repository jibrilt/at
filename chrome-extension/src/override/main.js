import Vue from 'vue';
import VueApollo from 'vue-apollo';
import { BootstrapVue } from 'bootstrap-vue';
import VScrollLock from 'v-scroll-lock'

import App from './App.vue';
import router from './router';
import apolloClient from '@/common/apolloClient';
import { currentUser } from '@/graphql/queries/auth';
import JwtService from "@/common/jwt.service";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueApollo);
Vue.use(VScrollLock);

const apolloProvider = new VueApollo({ defaultClient: apolloClient });

router.beforeEach((to, from, next) => {
  JwtService.getToken().then(authToken => {
    // Check authentication before each pageload
    let authTokenPresent = !!authToken;
    if (authTokenPresent) {
      new Promise(resolve => {
        apolloClient.query({
          query: currentUser,
          fetchPolicy: 'no-cache'
        }).then(({ data }) => {
          apolloClient.writeQuery({ query: currentUser, data });
          if(data.currentUser === null ) {
            JwtService.destroyToken();
            apolloClient.resetStore();
            apolloClient.writeData({
              data: {
                isAuthenticated: !!authToken
              }
            });
            return next({name: 'home'});
          }
        });
      });
    }

    if (to.meta.disableIfLoggedIn && !!authToken) {
      return next({name: 'login'});
    } else if (to.meta.loggedInOnly && !authToken) {
      return next({name: 'login'});
    }

    next();
  }, error => {
    console.log(error);
  });
});

router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = `${to.meta.title} - ActionItem Prototype`;
  });
});

new Vue({
  router,
  apolloProvider,
  apollo: {
    currentUser: currentUser
  },
  render: h => h(App)
}).$mount('#app');
