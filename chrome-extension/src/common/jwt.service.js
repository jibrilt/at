import { AUTH_TOKEN_KEY } from '@/common/config';

export const getToken = () => {
  return new Promise((resolve, reject) => {
    chrome.storage.local.get([AUTH_TOKEN_KEY], function(result) {
      resolve(result[AUTH_TOKEN_KEY]);
    });
  });
};

export const saveToken = token => {
  chrome.storage.local.set({[AUTH_TOKEN_KEY]: token}, function() {});
};

export const destroyToken = () => {
  chrome.storage.local.remove([AUTH_TOKEN_KEY], function() {});
};

export default { getToken, saveToken, destroyToken };
