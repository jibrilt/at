import { ActionCableLink } from 'graphql-ruby-client';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { split } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';

import { API_URL } from '@/common/config';
import JwtService from "@/common/jwt.service";
import { typeDefs, resolvers } from '@/graphql/resolvers';
import cable from '@/common/actionCableConsumer';

const httpLink = createHttpLink({ uri: `${API_URL}/graphql` });

const authLink = setContext(async (_, { headers }) => {
  const authToken = await JwtService.getToken().then(response => {
    return response;
  });

  return {
    headers: {
      ...headers,
      authorization: authToken ? `Bearer ${authToken}` : ''
    },
  };
});

const hasSubscriptionOperation = ({ query: { definitions } }) => {
  return definitions.some(
    ({ kind, operation }) => kind === 'OperationDefinition' && operation === 'subscription'
  );
};

const link = split(
  hasSubscriptionOperation,
  new ActionCableLink({cable}),
  authLink.concat(httpLink)
);

const cache = new InMemoryCache();

JwtService.getToken().then(getTokenResponse => {
  cache.writeData({
    data: {
      isAuthenticated: !!getTokenResponse,
      results: [],
      resultGroups: [],
      searchBackdrop: false,
      notificationsBackdrop: false
    },
  });
});

export default new ApolloClient({
  link: link,
  cache: cache,
  typeDefs,
  resolvers
});

