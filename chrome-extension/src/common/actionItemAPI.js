import Vue from 'vue'
import axios from 'axios';
import VueAxios from "vue-axios";
import Slack from "slack";

Vue.use(VueAxios, axios);

export default new class ActionItemAPI {

  constructor() {
    this.service = Vue.axios;
  }

  getProviderChannels(type, token) {
    if(type === 'slack') return Slack.conversations.list({token: token});
    return null;
  }

  getMessagesList(type, id, token) {
    if(type === 'google'){
      const path = `http://localhost:8091/gmail/v1/users/${id}/messages`;
      return this.service.get(path, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        }
      });
    }else{
      return null;
    }
  }

  postMessage(type, token, opts) {
    if(type === 'slack') return Slack.chat.postMessage({
      token: token,
      channel: opts.channel,
      text: opts.message
    });
    return null;
  }
}
