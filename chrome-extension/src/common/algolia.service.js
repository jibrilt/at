import algoliasearch from 'algoliasearch/lite';
import { ACTIONITEM_PROVIDERS } from '@/common/config';

const SEARCHABLE_ATTRIBUTES = [
  'body',
  'from',
  'from_email',
  'subject',
];

export const multiQuery = (apiKey, searchTerm) => {
  const client = algoliasearch(
    process.env.VUE_APP_ALGOLIA_APPLICATION_ID,
    apiKey
  );

  const index = client.initIndex('Message');
  const queries = [];

  queries.push({
    indexName: 'Message',
    query: searchTerm,
    restrictSearchableAttributes: SEARCHABLE_ATTRIBUTES
  });

  ACTIONITEM_PROVIDERS.forEach(provider => {
    queries.push({
      indexName: 'Message',
      query: searchTerm,
      restrictSearchableAttributes: SEARCHABLE_ATTRIBUTES,
      filters: `provider:${provider}`
    });
  });

  return client.multipleQueries(queries);
};

export const searchQuery = (apiKey, searchTerm, groupProvider, page) => {
  let query = {
    restrictSearchableAttributes: SEARCHABLE_ATTRIBUTES,
    page: page + 1
  };

  if (groupProvider !== 'All') {
    query.filters = `provider:${groupProvider}`;
  }

  const client = algoliasearch(
    process.env.VUE_APP_ALGOLIA_APPLICATION_ID,
    apiKey
  );

  const index = client.initIndex('Message');

  return index.search(searchTerm, query);
};

export const getAll = (apiKey, page) => {
  const client = algoliasearch(
    process.env.VUE_APP_ALGOLIA_APPLICATION_ID,
    apiKey
  );

  const index = client.initIndex('Message');

  let query = {
    restrictSearchableAttributes: SEARCHABLE_ATTRIBUTES,
    page: page
  };

  return index.search('', query);
}

export default { multiQuery, searchQuery, getAll };
