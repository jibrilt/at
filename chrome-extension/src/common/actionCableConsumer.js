import ActionCable from 'actioncable';
import JwtService from "@/common/jwt.service";

const getCableUrl = async () => {
  const protocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:';
  const host = 'localhost';
  const authToken = await JwtService.getToken().then(response => {
    return response;
  });

  return `${protocol}//${host}:3000/cable?token=${authToken}`;
};

const cable = ActionCable.createConsumer(getCableUrl());

getCableUrl().then(response => {
  Object.defineProperty(cable, 'url', {
    get: function() {
      return response;
    },
  });
}, error => {
});

export default cable;
