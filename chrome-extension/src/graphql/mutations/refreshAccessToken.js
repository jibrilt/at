import gql from 'graphql-tag';

const mutation = gql`
  mutation refreshAccessToken($uid: String!) {
    refreshAccessToken(input: { uid: $uid }) {
      success
      errors
    }
  }
`;

export default function refreshAccessToken({
   apollo,
   uid
  }) {
  return apollo.mutate({
    mutation,
    variables: {
      uid
    },
  });
}
