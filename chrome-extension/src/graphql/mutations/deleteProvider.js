import gql from 'graphql-tag';

const mutation = gql`
  mutation deleteProvider($uid: String!) {
    deleteProvider(input: { uid: $uid }) {
      success
      errors
    }
  }
`;

export default function deleteProvider({
  apollo,
  uid
}) {
  return apollo.mutate({
    mutation,
    variables: {
      uid
    },
  });
}
