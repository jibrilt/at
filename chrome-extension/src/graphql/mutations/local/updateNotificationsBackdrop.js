import gql from 'graphql-tag';

const mutation = gql`
  mutation updateNotificationsBackdrop($enabled: Boolean!) {
    updateNotificationsBackdrop(
      enabled: $enabled
    ) @client
  }
`;

export default function updateNotificationsBackdrop({
  apollo,
  enabled
}) {
  return apollo.mutate({
    mutation,
    variables: {
      enabled
    },
  });
}
