import gql from 'graphql-tag';

const mutation = gql`
  mutation removeTrckedItmLabel(
    $trackedItemId: String!,
    $tagId: String!
  ) {
    removeTrckedItmLabel(
      trackedItemId: $trackedItemId,
      tagId: $tagId
    ) @client
  }
`;

export default function removeTrckedItmLabel({
  apollo,
  trackedItemId,
  tagId
}) {
  return apollo.mutate({
    mutation,
    variables: {
      trackedItemId,
      tagId
    }
  });
}
