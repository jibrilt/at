import gql from 'graphql-tag';

const mutation = gql`
  mutation updateResultActive($id: String!, $active: Boolean!) {
    updateResultActive(
      id: $id,
      active: $active
    ) @client
  }
`;

export default function updateResultActive({
  apollo,
  id,
  active
}) {
  return apollo.mutate({
    mutation,
    variables: {
      id,
      active
    },
  });
}
