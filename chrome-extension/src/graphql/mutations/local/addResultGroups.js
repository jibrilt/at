import gql from 'graphql-tag';

const mutation = gql`
  mutation addResultGroups($groups: [ResultGroup]) {
    addResultGroups(
      groups: $groups
    ) @client
  }
`;

export default function addResultGroups({
  apollo,
  groups
}) {
  return apollo.mutate({
    mutation,
    variables: {
      groups
    },
  });
}
