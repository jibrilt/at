import gql from 'graphql-tag';

const mutation = gql`
  mutation registerUser(
    $email: String!,
    $password: String!,
  ) {
    registerUser(input: {
      email: $email,
      password: $password,
    }) {
      user {
        authenticationToken
      }
      success
      errors
    }
  }
`;

export default function({
  apollo,
  email,
  password,
}) {
  return apollo.mutate({
    mutation,
    variables: {
      email,
      password,
    }
  });
}
