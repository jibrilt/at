import gql from 'graphql-tag';

const mutation = gql`
  mutation abstract(
    $gqlId: String!
  ) {
    destroyTag(input: {
      gqlId: $gqlId
    }) {
      success
      errors
    }
  }
`;

export default function destroyTag({
  apollo,
  gqlId
}) {
  return apollo.mutate({
    mutation,
    variables: {
      gqlId
    },
  });
}
