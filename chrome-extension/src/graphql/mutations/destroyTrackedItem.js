import gql from 'graphql-tag';

const mutation = gql`
  mutation destroyTrackedItem($id: String!) {
    destroyTrackedItem(input: { id: $id }) {
      success
      errors
    }
  }
`;

export default function destroyTrackedItem({
  apollo,
  id
}) {
  return apollo.mutate({
    mutation,
    variables: {
      id
    },
  });
}
