import gql from 'graphql-tag';

const mutation = gql`
  mutation destroyUser($uid: String!) {
    destroyUser(input: { uid: $uid }) {
      success
      errors
    }
  }
`;

export default function destroyUser({
  apollo,
  uid
}) {
  return apollo.mutate({
    mutation,
    variables: {
      uid
    },
  });
}
