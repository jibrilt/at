import gql from 'graphql-tag';

const mutation = gql`
  mutation abstract(
    $name: String!
    $trackeditemGqlId: String!
  ) {
    createTag(input: {
      name: $name,
      trackeditemGqlId: $trackeditemGqlId
    }) {
      success
      errors
    }
  }
`;

export default function createTag({
  apollo,
  name,
  trackeditemGqlId
}) {
  return apollo.mutate({
    mutation,
    variables: {
      name,
      trackeditemGqlId
    },
  });
}
