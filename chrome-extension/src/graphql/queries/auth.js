import gql from 'graphql-tag';

export const currentUser = gql`
  query {
    currentUser {
      id
      email
      authenticationToken
      signedInVia
      algoliaApiKey
      providers {
        id
        uid
        name
        token
        botToken
      }
    }
  }
`;

export const isAuthenticated = gql`
  query isAuthenticated {
    isAuthenticated @client
  }
`;
