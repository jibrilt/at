import gql from 'graphql-tag';
import { trackedItemListFields } from '@/graphql/fragments/trackedItemListFields.js'

export const trackedItemsConnection = gql`
  ${trackedItemListFields}

  query abstract($afterCursor: String) {
    trackedItemsConnection(first: 10, after: $afterCursor) {
      pageInfo {
        endCursor
        hasNextPage
      }

      edges {
        cursor
        node {
          ...TrackedItemListFields
        }
      }
    }
  }
`;

export default { trackedItemsConnection };
