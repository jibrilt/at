import gql from 'graphql-tag';

export const results = gql`
  query {
    results @client {
      id
      subject
      senderName
      timeStamp
      body
      provider
      fromEmail
      deepLink
      active
      resultsGroup
      groupIndex
    }
  }
`;

export const resultGroups = gql`
  query resultGroups {
    resultGroups @client {
      id
      group
      page
      groupProvider
    }
  }
`;

export const searchBackdrop = gql`
  query searchBackdrop {
    searchBackdrop @client
  }
`;

export const notificationsBackdrop = gql`
  query notificationsBackdrop {
    notificationsBackdrop @client
  }
`;
