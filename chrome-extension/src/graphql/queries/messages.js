import gql from 'graphql-tag';

export const messages = gql`
  query {
    messages {
      trackedItemId
      providerMessageId
      deepLink
      body
      providerDatePosted
      gqlId
      userId
      provider
      from
      fromEmail
      subject
    }
  }
`;
